<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblProcesos;
use App\Models\TblPuntosAtencion;
use Illuminate\Support\Facades\Auth;
use App\Models\tbl_preguntas;
use App\Models\TblCuestionario;
use App\Models\TblPaciente;
use DB;

class dataController extends Controller
{
    public function deleteOldFiles()
    {
        $folder=public_path().DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
        $directorio = opendir($folder);
        while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
        {
            if (!is_dir($archivo))//verificamos si es o no un directorio
            {
                $name=str_replace('export','', $archivo);
                $name=substr($name,0,8);
                $fechaAnterior = strtotime(substr($name,0,4).'-'.substr($name,4,2).'-'.substr($name,6,2));
                $fecha_actual  = strtotime(date('Y-m-d'));
                
                if($fechaAnterior < $fecha_actual )
                {
                    unlink($folder.$archivo);
                }
            }
        }
        return [true];
    }
    public function procesosAll()
    {
        return ['validate'=>true,'data'=>TblProcesos::orderBy('nombre')->get()];
    }
    public function PuntosAtencionAll()
    {
        return ['validate'=>true,'data'=>TblPuntosAtencion::orderBy('nombre')->get()];
    }
    public function usersValidate()
    {
        if(is_null(Auth::id()))
        {
            return view('welcome');
        }
        else
        {
            return redirect('/dashboard/registro/new');
        }
    }
    public function generateExcel(Request $request)
    {
        try {
            
            function calcular_edad($fecha){
                $fecha_nac = new \DateTime(date('Y-m-d',strtotime($fecha))); // Creo un objeto DateTime de la fecha ingresada
                $fecha_hoy =  new \DateTime(date('Y-m-d',time())); // Creo un objeto DateTime de la fecha de hoy
                $edad = date_diff($fecha_hoy,$fecha_nac); // La funcion ayuda a calcular la diferencia, esto seria un objeto
                return "{$edad->format('%Y')} años {$edad->format('%m')} meses y {$edad->format('%d')} días";
            }
            
            $preguntas=tbl_preguntas::all();
            $paginas=1;
            $pagina=1;
            $table='';
            $respuestas = TblCuestionario::
            with('tblPaciente')->
            with('tblPaciente.tbl_documento_tipo')->
            with('tbl_cuestionario_x_respuesta')->
            with('tbl_cuestionario_x_respuesta.tbl_pregunta');
            if(trim($request->documento)!='')
            {
                $documento=str_replace('.','',$request->documento);
                $pacientes = TblPaciente::where('documento','LIKE','%'.trim($documento).'%')->pluck('id');
                $respuestas=$respuestas->whereIn('id_paciente',$pacientes);
            }
            $respuestas=$respuestas->get();
            
            $folder=public_path().DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
            if(!file_exists($folder))
            {
                mkdir($folder);
            }
            $name='export'.date('YmdHis').rand().'.csv';
            $file = fopen($folder.$name, "w");
            $header=[
                ('#'                            ),
                ('FECHA DE REGISTRO'         ),
                ('TIPO DE IDENTIFICACIÓN'    ),
                ('NUMERO DE IDENTIFICACION'  ),
                ('PRIMER NOMBRE'             ),
                ('SEGUNDO NOMBRE'            ),
                ('PRIMER APELLIDO'           ),
                ('SEGUNDO APELLIDO'          ),
                ('GENERO'                    ),
                ('FECHA DE NACIMIENTO'       ),
                ('EDAD'                      ),
                ('UNIDAD DE MEDIDA'          ),
                ('NACIONALIDAD'              ),
                ('DOMICILIO'                 ),
                ('DIRECCION'                 ),
                ('BARRIO /VEREDA'            ),
                ('ZANA RESIDENCIA'           ),
                ('TELEFONO'                  ),
                ('NOMBRE DE EPS'             ),
                ('OCUPACION'                 )
            ];
            $buscar=['¿','á','é','í','ó','ú','Á','É','Í','Ó','Ú',"\n","\r",'ñ','Ñ'];
            $reemplazar=['','a','e','i','o','u','A','E','I','O','U','','','n','N'];
            foreach($header as $key=>$temp)
            {
                $header[$key]=str_replace($buscar,$reemplazar,$temp);
            }
            foreach($preguntas as $temp)
            {
                $header[]=str_replace($buscar,$reemplazar,$temp->nombre);
            }
            fwrite($file,implode(';',$header).PHP_EOL);
            foreach($respuestas as $key=> $temp)
            {
                $temp1=[];
                $temp1[] = $key+1;
                $temp1[] = date('Y-m-d h:m:i A',strtotime($temp->created_at));
                $temp1[] = isset($temp->tblPaciente->tbl_documento_tipo->nombre) ? $temp->tblPaciente->tbl_documento_tipo->nombre:'';
                $temp1[] = isset($temp->tblPaciente->documento)?$temp->tblPaciente->documento:'';
                $temp1[] = isset($temp->tblPaciente->nombre_primero)?$temp->tblPaciente->nombre_primero:'';
                $temp1[] = isset($temp->tblPaciente->nombre_segundo)?$temp->tblPaciente->nombre_segundo:'';
                $temp1[] = isset($temp->tblPaciente->apellido_primero)?$temp->tblPaciente->apellido_primero:'';
                $temp1[] = isset($temp->tblPaciente->apellido_segundo)?$temp->tblPaciente->apellido_segundo:'';
                $temp1[] = isset($temp->tblPaciente->genero)?$temp->tblPaciente->genero:'';
                $temp1[] = !isset($temp->tblPaciente->fecha_nacimiento)?'':date('Y-m-d',strtotime($temp->tblPaciente->fecha_nacimiento));
                $temp1[] = !isset($temp->tblPaciente->fecha_nacimiento)?'':calcular_edad($temp->tblPaciente->fecha_nacimiento);
                $temp1[] = '';
                $temp1[] = '';
                $temp1[] = !isset($temp->tblPaciente->domicilio)?'':$temp->tblPaciente->domicilio;
                $temp1[] = !isset($temp->tblPaciente->direccion)?'':$temp->tblPaciente->direccion;
                $temp1[] = !isset($temp->tblPaciente->barrio)?'':$temp->tblPaciente->barrio.' '.$temp->tblPaciente->zona_residencia;
                $temp1[] = '';;
                $temp1[] = !isset($temp->tblPaciente->celular1)?'':(trim($temp->tblPaciente->celular1)==''?$temp->tblPaciente->celular2:$temp->tblPaciente->celular1);
                $temp1[] = '';//$temp->tblPaciente->id_eps;
                $temp1[] = !isset($temp->tblPaciente->ocupacion)?'':$temp->tblPaciente->ocupacion;
                foreach($temp->tbl_cuestionario_x_respuesta as $temp2)
                {
                    $temp1[]=$temp2->value;
                }
                foreach($temp1 as $key=>$row)
                {
                    $temp1[$key]=str_replace($buscar,$reemplazar,$row);
                }
                fwrite($file,implode(';',$temp1).PHP_EOL);
            }
            return response()->json(['validate'=>true,'data'=>['url'=>'temp/'.$name]]);
        } catch (\Throwable $th) {
            return response()->json(['validate'=>false,'data'=>$th->getMessage()],422);
        }
    }
    public function perfil()
    {
        $data = DB::select('SELECT 
        `roles`.`name` 
        FROM 
        `role_user` 
        INNER JOIN `roles`  ON `role_user`.`role_id` = `roles`.`id`
        WHERE `role_user`.`user_id`='.Auth::id());
        return $data;
    }
}
