@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4 >Cambiar contraseña</h4>
            </span>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('users.users.index') }}" class="btn btn-primary" title="Show All Users">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            @if(Session::has('success_message'))
                <div class="alert alert-success">
                    <span class="glyphicon glyphicon-ok"></span>
                    {!! session('success_message') !!}

                    <button type="button" class="close" data-dismiss="alert" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
            @endif

            <form method="POST" action="{{ route('users.users.changePass',$user->id) }}" accept-charset="UTF-8" id="create_users_form" name="create_users_form" class="form-horizontal">
            {{ csrf_field() }}
            

                <div class="form-group {{ $errors->has('old_pass') ? 'has-error' : '' }}">
                    <label for="old_pass" class="col-md-2 control-label">Ingrese la contraseña actual</label>
                    <div class="col-md-10">
                        <input class="form-control" name="old_pass" type="password" id="old_pass" minlength="1" maxlength="191" required="true">
                    </div>
                </div>


                <div class="form-group {{ $errors->has('new_pass_1') ? 'has-error' : '' }}">
                    <label for="new_pass_1" class="col-md-2 control-label">Ingrese la contraseña nueva</label>
                    <div class="col-md-10">
                        <input class="form-control" name="new_pass_1" type="password" id="new_pass_1" minlength="1" maxlength="191" required="true">
                    </div>
                </div>

                <div class="form-group {{ $errors->has('new_pass_2') ? 'has-error' : '' }}">
                    <label for="new_pass_2" class="col-md-2 control-label">Repita la contraseña nueva</label>
                    <div class="col-md-10">
                        <input class="form-control" name="new_pass_2" type="password" id="new_pass_2" minlength="1" maxlength="191" required="true">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection


