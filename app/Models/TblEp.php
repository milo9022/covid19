<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model;

/**
 * Class TblEp
 * 
 * @property int $id
 * @property string $descripcion
 *
 * @package App\Models
 */
class TblEp extends Model
{
	protected $table = 'tbl_eps';
	public $timestamps = false;

	protected $fillable = [
		'descripcion'
	];
}
