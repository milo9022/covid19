<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        //$this->call(TblProcesosSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TblDepartamentosTableSeeder::class);
        $this->call(TblDocumentoTiposTableSeeder::class);
        $this->call(TblMunicipiosTableSeeder::class);
        $this->call(TblPreguntasGruposTableSeeder::class);
        $this->call(TblPreguntasTableSeeder::class);
    }
}
