<?php

use Illuminate\Database\Seeder;

class TblPreguntasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_preguntas')->delete();
        
        \DB::table('tbl_preguntas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => '¿Ha viajado a zonas donde hay casos confirmados o probables de infeccion por COVID-19?',
                'nivel' => 1,
                'id_preguntas_grupos' => 1,
                'created_at' => '2020-03-20 11:21:29',
                'updated_at' => '2020-03-20 11:21:29',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => '¿Tuvo contacto cercano con un caso confirmado o probable de infeccion por COVID-19?',
                'nivel' => 1,
                'id_preguntas_grupos' => 1,
                'created_at' => '2020-03-20 11:21:55',
                'updated_at' => '2020-03-20 11:21:55',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'TOS',
                'nivel' => 1,
                'id_preguntas_grupos' => 2,
                'created_at' => '2020-03-20 11:22:10',
                'updated_at' => '2020-03-20 11:22:10',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'FIEBRE',
                'nivel' => 1,
                'id_preguntas_grupos' => 2,
                'created_at' => '2020-03-20 11:22:20',
                'updated_at' => '2020-03-20 11:22:20',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'FATIGA',
                'nivel' => 1,
                'id_preguntas_grupos' => 2,
                'created_at' => '2020-03-20 11:22:33',
                'updated_at' => '2020-03-20 11:22:33',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'DOLOR DE GARGANTA',
                'nivel' => 1,
                'id_preguntas_grupos' => 2,
                'created_at' => '2020-03-20 11:22:46',
                'updated_at' => '2020-03-20 11:22:46',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'DIFICULTAD PARA RESPIRAR',
                'nivel' => 1,
                'id_preguntas_grupos' => 2,
                'created_at' => '2020-03-20 11:23:01',
                'updated_at' => '2020-03-20 11:23:01',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'COMBORBILIDADES O FACTORES DE RIESGO',
                'nivel' => 1,
                'id_preguntas_grupos' => 3,
                'created_at' => '2020-03-20 15:11:03',
                'updated_at' => '2020-03-20 15:11:03',
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'Observaciones',
                'nivel' => 2,
                'id_preguntas_grupos' => 3,
                'created_at' => '2020-03-20 15:15:34',
                'updated_at' => '2020-03-20 15:15:34',
            ),
        ));
        
        
    }
}