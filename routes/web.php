<?php
Auth::routes();
Route::get('/', 'dataController@usersValidate');
Route::get('/preguntas_grupos/all','TblPreguntasGruposController@all');
Route::get( '/preguntas/api', 'TblPreguntasController@pacientes');
Route::post('/preguntas/api/save', 'TblPreguntasController@respuestasSave');
Route::get('/clear','dataController@deleteOldFiles');
Route::group(['middleware' => 'auth'], function() 
{
     Route::get('/dashboard/{any}', function () {return view('welcome');})->where('any', '.*');
     Route::get('/dashboard', function () {return redirect('/dashboard/registro/new');});

     Route::get('/logout', 'UserController@Cerrar_Logout')->name('exit');
     Route::get('/procesos', 'dataController@procesosAll');
     Route::get('/puntosatencion', 'dataController@PuntosAtencionAll');
     Route::post('/export', 'dataController@generateExcel');
     Route::get('/perfil', 'dataController@perfil');

     Route::group(['prefix' => 'limpiar'], function() {
          Route::get('/',function(){
               Artisan::call('cache:clear');
               Artisan::call('config:cache');
               Artisan::call('config:clear');
               Artisan::call('view:clear');
               Artisan::call('route:clear');
               Artisan::call('clear-compiled');
               Artisan::call('key:generate');
               return response()->json(['validate'=>true,'msj'=>'clear success']);
          });
          Route::get('/migrate',function(){
               Artisan::call('migrate');
               return response()->json(['validate'=>true,'msj'=>'migrate success']);
          });
     });
     Route::group(['prefix' => 'preguntas'], function () {    
          Route::get('/', 'TblPreguntasController@index')
          ->name('tbl_preguntas.tbl_preguntas.index');
          Route::get('/respuestas', 'TblPreguntasController@respuestas')
          ->name('tbl_preguntas.tbl_preguntas.respuestas');
          Route::get('/respuestas/all','TblPreguntasController@verRespuestas');
          Route::get('/respuestas/draw','TblPreguntasController@dataGrafica');
          
          Route::get('/create','TblPreguntasController@create')
               ->name('tbl_preguntas.tbl_preguntas.create');
          Route::get('/show/{tblPreguntas}','TblPreguntasController@show')
               ->name('tbl_preguntas.tbl_preguntas.show');
          Route::get('/{tblPreguntas}/edit','TblPreguntasController@edit')
               ->name('tbl_preguntas.tbl_preguntas.edit');
          Route::post('/', 'TblPreguntasController@store')
               ->name('tbl_preguntas.tbl_preguntas.store');
          Route::put('tbl_preguntas/{tblPreguntas}', 'TblPreguntasController@update')
               ->name('tbl_preguntas.tbl_preguntas.update');
          Route::delete('/tbl_preguntas/{tblPreguntas}','TblPreguntasController@destroy')
               ->name('tbl_preguntas.tbl_preguntas.destroy');
     });
     Route::group(['prefix' => 'preguntas_grupos'], function () {
          Route::get('/', 'TblPreguntasGruposController@index')
               ->name('tbl_preguntas_grupos.tbl_preguntas_grupos.index');
          Route::get('/create','TblPreguntasGruposController@create')
               ->name('tbl_preguntas_grupos.tbl_preguntas_grupos.create');
          Route::get('/show/{tblPreguntasGrupos}','TblPreguntasGruposController@show')
               ->name('tbl_preguntas_grupos.tbl_preguntas_grupos.show');
          Route::get('/{tblPreguntasGrupos}/edit','TblPreguntasGruposController@edit')
               ->name('tbl_preguntas_grupos.tbl_preguntas_grupos.edit');
          Route::post('/', 'TblPreguntasGruposController@store')
          ->name('tbl_preguntas_grupos.tbl_preguntas_grupos.store');
          Route::put('tbl_preguntas_grupos/{tblPreguntasGrupos}', 'TblPreguntasGruposController@update')
          ->name('tbl_preguntas_grupos.tbl_preguntas_grupos.update');
          Route::delete('/tbl_preguntas_grupos/{tblPreguntasGrupos}','TblPreguntasGruposController@destroy')
          ->name('tbl_preguntas_grupos.tbl_preguntas_grupos.destroy');
     });
     Route::resource('user', 'UserController');
     Route::post('user/reloadpass', 'UserController@reloadPass');
});