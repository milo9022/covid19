require('./bootstrap')

import Vue from 'vue'
import Ionic from '@ionic/vue';
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import HighchartsVue from 'highcharts-vue'
import VueSweetalert2 from 'vue-sweetalert2';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import '@ionic/core/css/ionic.bundle.css';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(HighchartsVue)
Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(Ionic);
Vue.use(VueSweetalert2);
Vue.config.productionTip = false;

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)


import App from './view/template/layout'
import registroNew  from './view/registros/new'
import registroIndex  from './view/registros/index'
import registroPaciente  from './view/registros/paciente'
import UserIndex  from './view/user/index'
import UserNew  from './view/user/create'

let pref="/dashboard";
const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', name:'registro_pacinete', component:registroPaciente},
        { path: pref+'/', redirect:'/registro/new'},
        { path: pref+'/registro/new', name:'registro_new', component: registroNew },
        { path: pref+'/registro/', name:'registro_index', component: registroIndex },
        { path: pref+'/user/new', name:'user_new', component: UserNew },
        { path: pref+'/user/', name:'user_index', component: UserIndex },
    ]
});
new Vue({
    router,
    components: { App },
    el: 'app',
});
