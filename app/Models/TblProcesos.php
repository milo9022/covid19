<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblProcesos extends Model
{
    protected $table = 'tbl_procesos';

	protected $fillable = ['nombre'];
}
