@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4 class="mt-5 mb-5">Create New Tbl Preguntas Grupos</h4>
            </span>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_preguntas_grupos.tbl_preguntas_grupos.index') }}" class="btn btn-primary" title="Show All Tbl Preguntas Grupos">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('tbl_preguntas_grupos.tbl_preguntas_grupos.store') }}" accept-charset="UTF-8" id="create_tbl_preguntas_grupos_form" name="create_tbl_preguntas_grupos_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('tbl_preguntas_grupos.form', [
                                        'tblPreguntasGrupos' => null,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection


