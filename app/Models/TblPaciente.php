<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model;

/**
 * Class TblPaciente
 * 
 * @property int $id
 * @property string $nombre_primero
 * @property string $nombre_segundo
 * @property string $apellido_primero
 * @property string $apellido_segundo
 * @property Carbon $fecha_nacimiento
 * @property int $id_documento_tipo
 * @property string $genero
 * @property int $id_nacionalidad
 * @property string $documento
 * @property string $domicilio
 * @property string $direccion
 * @property string $barrio
 * @property string $zona_residencia
 * @property int $id_eps
 * @property string $celular1
 * @property string $celular2
 * @property string $email
 * @property string $ocupacion
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblPaciente extends Model
{
	protected $table = 'tbl_pacientes';

	protected $casts = [
		'id_documento_tipo' => 'int',
		'id_nacionalidad' => 'int',
		'id_eps' => 'int'
	];

	protected $dates = [
		'fecha_nacimiento'
	];

	protected $fillable = [
		'nombre_primero',
		'nombre_segundo',
		'apellido_primero',
		'apellido_segundo',
		'fecha_nacimiento',
		'id_documento_tipo',
		'genero',
		'id_nacionalidad',
		'documento',
		'domicilio',
		'direccion',
		'barrio',
		'zona_residencia',
		'id_eps',
		'celular1',
		'celular2',
		'email',
		'ocupacion'
	];
	public function tbl_documento_tipo()
	{
		return $this->belongsTo(TblDocumentoTipo::class, 'id_documento_tipo');
	}
}
