<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        \DB::table('tbl_procesos')->delete();
        
        \DB::table('tbl_procesos')->insert(array ( 
            array (
                'nombre'=>'Asignación de citas',
                'created_at' => date('Y-m-d h:i:s')
            ),
            array (
                'nombre'=>'CAP',
                'created_at' => date('Y-m-d h:i:s')
            ),
            )
        );
        $user = new User();
        $user->id = 1;
        $user->nombre_primero = 'admin';
        $user->nombre_segundo = 'admin';
        $user->apellido_primero = 'admin';
        $user->apellido_segundo = 'admin';
        $user->documento = '1';
        $user->id_punto_atencion = 1;
        $user->activo = 1;
        $user->id_proceso=null;
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('admin');
        $user->login = ('admin');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'admin')->first()); 

        $user = new User();
        $user->nombre_primero = 'Sandra';
        $user->nombre_segundo = 'Milena';
        $user->apellido_primero = 'Orozco';
        $user->apellido_segundo = '';
        $user->documento = '4';
        $user->id_punto_atencion = 1;
        $user->id_proceso=1;
        $user->activo = 1;
        $user->email = 'sorozco@esepopayan.gov.co';
        $user->password = bcrypt('sorozco');
        $user->login = ('sorozco');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'supervision')->first()); 

        $user = new User();
        $user->nombre_primero = 'Marlith';
        $user->nombre_segundo = 'Lorena';
        $user->apellido_primero = 'galvis';
        $user->apellido_segundo = 'calambas';
        $user->documento = '5';
        $user->id_punto_atencion = 1;
        $user->id_proceso=1;
        $user->activo = 1;
        $user->email = 'mgalvis@esepopayan.gov.co';
        $user->password = bcrypt('mgalvis');
        $user->login = ('mgalvis');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'supervision')->first()); 
        
        $user = new User();
        $user->nombre_primero = 'Carolina';
        $user->nombre_segundo = '';
        $user->apellido_primero = 'Ramirez';
        $user->apellido_segundo = 'Higuera';
        $user->documento = '6';
        $user->id_punto_atencion = 1;
        $user->id_proceso=1;
        $user->activo = 1;
        $user->email = 'cramirez@esepopayan.gov.co';
        $user->password = bcrypt('cramirez');
        $user->login = ('cramirez');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'supervision')->first()); 
        
        $user = new User();
        $user->nombre_primero = 'Ingrid';
        $user->nombre_segundo = 'Kanna';
        $user->apellido_primero = 'Piamba';
        $user->apellido_segundo = 'u';
        $user->documento = '7';
        $user->id_punto_atencion = 1;
        $user->id_proceso=1;
        $user->activo = 1;
        $user->email = 'ipiamba@esepopayan.gov.co';
        $user->password = bcrypt('ipiamba');
        $user->login = ('ipiamba');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'supervision')->first()); 

        $user = new User();
        $user->nombre_primero = 'Liliana';
        $user->nombre_segundo = 'Elena';
        $user->apellido_primero = 'Hurtado';
        $user->apellido_segundo = 'Acosta';
        $user->documento = '8';
        $user->id_punto_atencion = 1;
        $user->id_proceso=1;
        $user->activo = 1;
        $user->email = 'ehurtado@esepopayan.gov.co';
        $user->password = bcrypt('ehurtado');
        $user->login = ('ehurtado');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'supervision')->first()); 



        $user = new User();
        $user->nombre_primero = 'Yuli';
        $user->nombre_segundo = '';
        $user->apellido_primero = 'Vega';
        $user->apellido_segundo = '';
        $user->documento = '9';
        $user->id_punto_atencion = 1;
        $user->id_proceso=2;
        $user->activo = 1;
        $user->email = 'yvega@esepopayan.gov.co';
        $user->password = bcrypt('3474');
        $user->login = ('yvega');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'supervision')->first()); 

        $user = new User();
        $user->nombre_primero = 'Dayana';
        $user->nombre_segundo = '';
        $user->apellido_primero = 'Laso';
        $user->apellido_segundo = '';
        $user->documento = '10';
        $user->id_punto_atencion = 1;
        $user->id_proceso=2;
        $user->activo = 1;
        $user->email = 'llaso@esepopayan.gov.co';
        $user->password = bcrypt('5381');
        $user->login = ('llaso');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'supervision')->first()); 
        
        $user = new User();
        $user->nombre_primero = 'F';
        $user->nombre_segundo = '';
        $user->apellido_primero = 'Patiño';
        $user->apellido_segundo = '';
        $user->documento = '10';
        $user->id_punto_atencion = 1;
        $user->id_proceso=2;
        $user->activo = 1;
        $user->email = 'fpatiño@esepopayan.gov.co';
        $user->password = bcrypt('EsePopayan');
        $user->login = ('fpatiño');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'supervision')->first());
        
    }
}