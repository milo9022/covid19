<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model;

/**
 * Class TblDepartamento
 * 
 * @property int $id
 * @property string $nombre
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblDepartamento extends Model
{
	protected $table = 'tbl_departamentos';

	protected $fillable = [
		'nombre'
	];
}
