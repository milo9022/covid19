<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTblCuestionarioXRespuestaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbl_cuestionario_x_respuesta', function(Blueprint $table)
		{
			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('id_cuestionario')->unsigned()->index('fk_tbl_cuestionario_tbl_cuestionario_x_respuesta');
			$table->bigInteger('id_pregunta')->unsigned()->index('fk_tbl_preguntas_tbl_cuestionario_x_respuesta');
			$table->string('value');
			$table->integer('id_user')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbl_cuestionario_x_respuesta');
	}

}
