<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrator';
        $role->save();

        $role = new Role();
        $role->name = 'supervision';
        $role->description = 'supervision';
        $role->save();
        
        $role = new Role();
        $role->name = 'facturador';
        $role->description = 'facturador';
        $role->save();
        
    }
}
