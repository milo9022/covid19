<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        try
        {
            foreach($roles as $role) 
            {
                if($request->user()->hasRole($role))
                {
                    return $next($request);
                }
            }
            abort(403, "No tienes autorización para ingresar a este módulo.");
        }
        catch (\Throwable $th) 
        {
            abort(403, "No tienes autorización para ingresar.");
        }
    }
}
