<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\tbl_preguntas;
use App\Models\tbl_preguntas_grupos;
use App\Models\TblPaciente;
use App\Models\TblCuestionarioXRespuestum;
use App\Models\TblCuestionario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class TblPreguntasController extends Controller
{

    /**
     * Display a listing of the tbl preguntas.
     *
     * @return Illuminate\View\View
     */
    public function pacientes(Request $request)
    {
        function callAPI($url, $data=false){
            $curl = curl_init();
            if ($data)
            {
                $url = sprintf("%s?%s", $url, http_build_query($data));
            }
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            // EXECUTE:
            $result = curl_exec($curl);
            if(!$result){die("Connection Failure");}
            curl_close($curl);
            return $result;
        }
        try {
            $data=callAPI('http://citas.esepopayan.gov.co/api/paciente/get?key='.(md5('citas'.date('Ymd'))).'&documento='.$request->documento);
            return ['validate'=>true,'data'=>json_decode($data)];
        } catch (\Throwable $th) {
            return ['validate'=>false,'data'=>$th->getMessage()];
        }
        
    }
    public function respuestasSave(Request $request)
    {
        $pac = TblPaciente::where('documento','=',trim($request->paciente['documento']))->first();
        $pac = is_null($pac) ? new TblPaciente():TblPaciente::find($pac->id);
        if(is_null(Auth::id()))
        {
            if(!is_null($request->data)>0)
            {
                $pac->nombre_primero    = $request->paciente['data']['nombre_primero'];
                $pac->nombre_segundo    = $request->paciente['data']['nombre_segundo'];
                $pac->apellido_primero  = $request->paciente['data']['apellido_primero'];
                $pac->apellido_segundo  = $request->paciente['data']['apellido_segundo'];
                $pac->fecha_nacimiento  = $request->paciente['data']['fecha_nacimiento'];
                $pac->id_documento_tipo = $request->paciente['data']['id_documento_tipo'];
                $pac->documento         = $request->paciente['data']['documento'];
                $pac->id_eps            = $request->paciente['data']['id_eps'];
                $pac->celular1          = $request->paciente['data']['celular1'];
                $pac->celular2          = $request->paciente['data']['celular2'];
                $pac->email             = $request->paciente['data']['email'];
                $pac->genero            = ($request->paciente['data']['id_sexo']==1?'Masculino':'Femenino');
                $pac->save();
            }
            else{
                $pac->documento = $request->paciente['documento'];
                $pac->celular1  = $request->paciente['celular'];
                $pac->direccion = $request->paciente['direccion'];
                $pac->email     = $request->paciente['email'];
                $pac->save();
            }
        }
        else{
            $pac->nombre_primero    = $request->paciente['nombre_primero'];
            $pac->nombre_segundo    = $request->paciente['nombre_segundo'];
            $pac->apellido_primero  = $request->paciente['apellido_primero'];
            $pac->apellido_segundo  = $request->paciente['apellido_segundo'];
            $pac->fecha_nacimiento  = $request->paciente['fecha_nacimiento'];
            $pac->id_documento_tipo = $request->paciente['id_documento_tipo'];
            $pac->documento         = $request->paciente['documento'];
            $pac->id_eps            = $request->paciente['id_eps'];
            $pac->celular1          = $request->paciente['celular1'];
            $pac->celular2          = $request->paciente['celular2'];
            $pac->email             = $request->paciente['email'];
            $pac->genero            = ($request->paciente['id_sexo']==1?'Masculino':'Femenino');
            $pac->save();
        }
        $cuest = new TblCuestionario();
        $cuest->id_paciente=$pac->id;
        $cuest->id_user=Auth::id();
        $cuest->save();
        foreach($request->respuestas as $temp)
        {
            foreach($temp['tbl_preguntas'] as $value)
            {
                $data = new TblCuestionarioXRespuestum();
                $data->id_cuestionario = $cuest->id;
                $data->id_pregunta=$value['id'];
                $temp1='';
                switch($value['nivel'])
                {
                    case '1':
                        $temp1=$value['value']?'Si':'No';
                    break;
                    default:
                        $temp1=trim($value['value']);

                    break;
                }
                $data->value=$temp1;
                $data->save();
            }
        }
        return ['validate'=>true];

    }
    
    public function index()
    {
        $tblPreguntasObjects = tbl_preguntas::with('tblGrupos')->paginate(25);

        return view('tbl_preguntas.index', compact('tblPreguntasObjects'));
    }
    public function dataGrafica()
    {
        $series = array();
        $preguntas=tbl_preguntas::all();
        $respuestas = TblCuestionario::
        with(['tbl_cuestionario_x_respuesta'=>function($q)
        {
            $q->where('value','=','Si');
        }])->
        get();
        foreach($preguntas as $value)
        {
            $series[]=[
                'id'=>$value->id,
                'name'=>$value->nombre,
                'data'=>[0]
            ];
        }
        foreach($series as $idx=>$value1)
        {
            foreach($respuestas as $value)
            {
                $res = array_search($value1['id'], array_column((json_decode(json_encode($value->tbl_cuestionario_x_respuesta))), 'id_pregunta'));
                //var_dump($res);
                if($res !==FALSE)
                {
                    $series[$idx]['data'][0]=$series[$idx]['data'][0]+1;
                }
            }
        }
        return ['validate'=>true,'data'=>['series'=>$series]];
    }
    public function verRespuestas(Request $request)
    {
        $preguntas=tbl_preguntas::all();
        $paginas=1;
        $pagina=1;
        $respuestas = TblCuestionario::
        with('tblPaciente')->
        with('tblPaciente.tbl_documento_tipo')->
        with('tbl_cuestionario_x_respuesta')->
        with('tbl_cuestionario_x_respuesta.tbl_pregunta');
        if(trim($request->documento)!='')
        {
            $documento=str_replace('.','',$request->documento);
            $pacientes = TblPaciente::where('documento','LIKE','%'.trim($documento).'%')->pluck('id');
            $respuestas=$respuestas->whereIn('id_paciente',$pacientes);
        }
        if(is_null($request->pag))
        {
            $respuestas=$respuestas->get();
        }
        else
        {
            $limit      =$request->registros;
            $pagina     =$request->pag;
            $respuestas =
            $respuestas->
            limit($limit)->
            offset(($request->pag - 1) * $limit)->
            get();
            $count=0;
            if(trim($request->documento)!='')
            {
                $documento=str_replace('.','',$request->documento);
                $pacientes = TblPaciente::where('documento','LIKE','%'.trim($documento).'%')->pluck('id');
                $count=TblCuestionario::whereIn('id_paciente',$pacientes)->count();
            }
            else{
                $count=TblCuestionario::count();
            }
            $paginas = ceil($count/$limit);
        }
        return ['validate'=>true,'data'=>['respuestas'=>$respuestas,'preguntas'=>$preguntas,'paginas'=>$paginas,'pagina'=>$pagina]];
    }
    public function respuestas()
    {
        $respuestas = TblCuestionario::
        with('tblPaciente')->
        with('tblPaciente.tbl_documento_tipo')->
        with('tbl_cuestionario_x_respuesta')->
        with('tbl_cuestionario_x_respuesta.tbl_pregunta')->
        get();
        
        return view('tbl_respuestas.index', compact('respuestas'));
    }
    

    /**
     * Show the form for creating a new tbl preguntas.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        $tblPreguntasGruposObjects = tbl_preguntas_grupos::all();
        return view('tbl_preguntas.create',compact('tblPreguntasGruposObjects'));
    }

    /**
     * Store a new tbl preguntas in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            tbl_preguntas::create($data);

            return redirect()->route('tbl_preguntas.tbl_preguntas.index')
                ->with('success_message', 'Tbl Preguntas was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified tbl preguntas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblPreguntas = tbl_preguntas::findOrFail($id);

        return view('tbl_preguntas.show', compact('tblPreguntas'));
    }

    /**
     * Show the form for editing the specified tbl preguntas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tblPreguntasGruposObjects = tbl_preguntas_grupos::all();
        $tblPreguntas = tbl_preguntas::with('tblGrupos')->findOrFail($id);
        return view('tbl_preguntas.edit', compact('tblPreguntas','tblPreguntasGruposObjects'));
    }

    /**
     * Update the specified tbl preguntas in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblPreguntas = tbl_preguntas::findOrFail($id);
            $tblPreguntas->update($data);

            return redirect()->route('tbl_preguntas.tbl_preguntas.index')
                ->with('success_message', 'Tbl Preguntas was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified tbl preguntas from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblPreguntas = tbl_preguntas::findOrFail($id);
            $tblPreguntas->delete();

            return redirect()->route('tbl_preguntas.tbl_preguntas.index')
                ->with('success_message', 'Tbl Preguntas was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre' => 'required|string|min:1|max:191',
            'nivel' => 'nullable|numeric|min:-2147483648|max:2147483647',
            'id_preguntas_grupos' => 'required|string|min:1', 
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
