@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Tbl Preguntas Grupos</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_preguntas_grupos.tbl_preguntas_grupos.create') }}" class="btn btn-success" title="Create New Tbl Preguntas Grupos">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($tblPreguntasGruposObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Tbl Preguntas Grupos Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tblPreguntasGruposObjects as $key => $tblPreguntasGrupos)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{ $tblPreguntasGrupos->nombre }}</td>
                            <td>
                                <form method="POST" action="{!! route('tbl_preguntas_grupos.tbl_preguntas_grupos.destroy', $tblPreguntasGrupos->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('tbl_preguntas_grupos.tbl_preguntas_grupos.show', $tblPreguntasGrupos->id ) }}" class="btn btn-info" title="Show Tbl Preguntas Grupos">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('tbl_preguntas_grupos.tbl_preguntas_grupos.edit', $tblPreguntasGrupos->id ) }}" class="btn btn-primary" title="Edit Tbl Preguntas Grupos">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Tbl Preguntas Grupos" onclick="return confirm(&quot;Click Ok to delete Tbl Preguntas Grupos.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $tblPreguntasGruposObjects->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection