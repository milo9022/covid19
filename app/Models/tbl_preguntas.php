<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_preguntas extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_preguntas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'nombre',
                  'nivel',
                  'id_preguntas_grupos'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the tblCuestionarioXRespuestum for this model.
     *
     * @return App\Models\TblCuestionarioXRespuestum
     */
    public function tblCuestionarioXRespuestum()
    {
        return $this->hasOne('App\Models\TblCuestionarioXRespuestum','id_pregunta','id');
    }

    public function tblGrupos()
    {
        return $this->belongsTo('App\Models\tbl_preguntas_grupos', 'id_preguntas_grupos');
    }

}
