@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Tbl Preguntas' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('tbl_preguntas.tbl_preguntas.destroy', $tblPreguntas->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('tbl_preguntas.tbl_preguntas.index') }}" class="btn btn-primary" title="Show All Tbl Preguntas">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('tbl_preguntas.tbl_preguntas.create') }}" class="btn btn-success" title="Create New Tbl Preguntas">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('tbl_preguntas.tbl_preguntas.edit', $tblPreguntas->id ) }}" class="btn btn-primary" title="Edit Tbl Preguntas">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Tbl Preguntas" onclick="return confirm(&quot;Click Ok to delete Tbl Preguntas.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre</dt>
            <dd>{{ $tblPreguntas->nombre }}</dd>
            <dt>Nivel</dt>
            <dd>{{ $tblPreguntas->nivel }}</dd>
            <dt>Id Preguntas Grupos</dt>
            <dd>{{ $tblPreguntas->id_preguntas_grupos }}</dd>
            <dt>Created At</dt>
            <dd>{{ $tblPreguntas->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $tblPreguntas->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection