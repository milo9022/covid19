function CreateSelect(data,value,name)
{
    html=""
    $.each(data,function(i,val)
    {
        html=html+'<option value="'+val[value]+'">'+val[name]+'</option>\n';
    })
    return html;

}
function changeDepartament()
{
    $.ajax({
        url:base+'/api/municipios',
        type:'POST',
        async:false,
        data:{id_departamento:$('#id_departamento').val()},
        dataType:'json',
        success:function(response)
        {   
            var res=CreateSelect(response,'id','nombre');
            $('#id_municipio').html(res);
            if(id_municipio!=null)
            {
                $('#id_municipio').val(id_municipio);
            }
        }

    })
}
function searchDepartamentos()
{
    var id_municipio = $('#id_municipio').val();
    if(id_municipio!=null)
    {
        changeDepartament();
         $('#id_municipio').val(id_municipio);
    }
    $('#id_departamento').change(function()
    {
        changeDepartament(); 
    });
}
$(function()
{
    searchDepartamentos();
})