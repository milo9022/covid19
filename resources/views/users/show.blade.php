@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 >{{ isset($title) ? $title : 'Users' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('users.users.destroy', $users->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('users.users.index') }}" class="btn btn-primary" title="Show All Users">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('users.users.create') }}" class="btn btn-success" title="Create New Users">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('users.users.edit', $users->id ) }}" class="btn btn-primary" title="Edit Users">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Users" onclick="return confirm(&quot;Click Ok to delete Users.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre Primero</dt>
            <dd>{{ $users->nombre_primero }}</dd>
            <dt>Nombre Segundo</dt>
            <dd>{{ $users->nombre_segundo }}</dd>
            <dt>Apellido Primero</dt>
            <dd>{{ $users->apellido_primero }}</dd>
            <dt>Apellido Segundo</dt>
            <dd>{{ $users->apellido_segundo }}</dd>
            <dt>Documento</dt>
            <dd>{{ $users->documento }}</dd>
            <dt>Id Punto Atencion</dt>
            <dd>{{ $users->id_punto_atencion }}</dd>
            <dt>Activo</dt>
            <dd>{{ $users->activo }}</dd>
            <dt>Email</dt>
            <dd>{{ $users->email }}</dd>
            <dt>Email Verified At</dt>
            <dd>{{ $users->email_verified_at }}</dd>
            <dt>Password</dt>
            <dd>{{ $users->password }}</dd>
            <dt>Remember Token</dt>
            <dd>{{ $users->remember_token }}</dd>
            <dt>Created At</dt>
            <dd>{{ $users->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $users->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection