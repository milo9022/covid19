<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ['validate'=>true,'data'=>User::all()];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->nombre_primero = $request->nombre_primero;
        $user->nombre_segundo = $request->nombre_segundo;
        $user->apellido_primero = $request->apellido_primero;
        $user->apellido_segundo = $request->apellido_segundo;
        $user->documento = $request->documento;
        $user->id_punto_atencion = $request->id_punto_atencion;
        $user->id_proceso= $request->id_proceso;
        $user->activo = $request->activo;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->login = $request->login;
        $user->save();
        $user->roles()->attach(Role::where('id', 2)->first());
        return ['validate'=>true];
    }
    public function reloadPass(Request $request)
    { 
        $user = User::find($request->id);
        $user->password = bcrypt($user->documento);
        $user->save();
        return ['validate'=>true];
    }
    public function Cerrar_Logout()
    {
        Auth::logout();
        return redirect()->route('login')
        ->with('msg', 'Gracias por visitarnos!.');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
