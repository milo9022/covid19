<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Iniciar sesion</title>
		<link rel="stylesheet" href="{{url('/css/login.css')}}">
		<link rel="stylesheet" href="{{url('/css/bootstrap/bootstrap.min.css')}}">
		<script src="{{url('/js/jquery.min.js')}}"></script>
		<script src="{{url('/js/bootstrap/bootstrap.min.js')}}"></script>
		<!------ Include the above in your HEAD tag ---------->
	</head>
	<body>
	<br>
	<br>
	<br>
		<div class="wrapper fadeInDown">
			<div id="formContent">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-8">
							<div class="card">
								<div class="card-header" style="background-color: #00ABEF;color: #FFF;">Sistema de alerta por COVID19</div>
								<div class="card-body">
										<div class="container-fluid">
											<div class="row">
											<div class="col-md-2">
											<img style="width: 180px;" src="{{url('img/logo.jpg')}}">
											</div>
											<div class="col-md-10">
												<form method="POST" action="{{ route('login') }}">
													@csrf
													<div class="form-group row">
														<label for="login" class="col-md-4 col-form-label text-md-right">Login</label>
														<div class="col-md-6">
															<input id="login" type="text" class="form-control @error('login') is-invalid @enderror" name="login" value="{{ old('login') }}" required autocomplete="login" autofocus>
															@error('login')
															<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
															</span>
															@enderror
														</div>
													</div>
													<div class="form-group row">
														<label for="password" class="col-md-4 col-form-label text-md-right">Contrase&#241;a</label>
														<div class="col-md-6">
															<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
															@error('password')
															<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
															</span>
															@enderror
														</div>
													</div>
													<div class="form-group row">
														<div class="col-md-6 offset-md-4">
															<div class="form-check">
																<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
																<label class="form-check-label" for="remember">
																Recordarme
																</label>
															</div>
														</div>
													</div>
													<div class="form-group row mb-0">
														<div class="col-md-8 offset-md-4">
															<button type="submit" class="btn btn-primary">
																Iniciar
															</button>
														</div>
													</div>
												</form>
											</div>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



<!-- NUEVO BODY-->
		<style type="text/css">
			    .ui-panelgrid td, .ui-panelgrid tr
						{
						    border-style: none !important
						}
					body,
					html {
						margin:0;
						padding:0;
						color:#000;
						background:#F4F3F3;
						/*background-image: url("../images/ESEfondo2.jpg");*/
					}
					#wrap {
						width:1050px;
						height:515px;
						margin:0 auto;
						/*background:#F4F3F3;*/
						background-image: url("../images/ESEfondo2.jpg");
					}
					#header {
				    	padding:5px 10px;
						background-image: url('../images/ESEfondo2.jpg');
						border-radius: 2px;box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.4), 0px 0px 4px white inset, 2px 2px 2px rgba(0, 0, 0, 0.2);
						border-top: 0px solid #ccc;
					    font-size: 11px; text-align: center;
					}
					h1 {
					    margin:0;
				    }
					
					
					#main {
						float:left;
						width:400px;
						height:365px;
						padding:10px;
						background:#ffffff;
						
					}
						
					h2 {
						margin:0 0 1em;
					}
					#sidebar {
						float:left;
						width:610px;
						height:365px;
						padding:10px;
						margin-top:0px;
						
						background:#ffffff;
					}
					
					#footer {
						clear:both;
						padding:10px 10px;
						background-image: url('../images/ESEfondo2.jpg');
						border-radius: 2px;box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.4), 0px 0px 4px white inset, 2px 2px 2px rgba(0, 0, 0, 0.2);
						border-top: 0px solid #ccc;
					    font-size: 10px; text-align: center;
					}
					#footer p {
						margin:0;
				    }
					* html #footer {
						height:1px;
					}
			</style>
	</body>
</html>