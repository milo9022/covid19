<?php

use Illuminate\Database\Seeder;

class TblMunicipiosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_municipios')->delete();
        
        \DB::table('tbl_municipios')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'SUAN',
                'id_departamento' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'USIACURI',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'BARANOA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'PUERTO COLOMBIA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'SOLEDAD',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'JUAN DE ACOSTA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'TUBARA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'GALAPA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'SANTA LUCIA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'SABANAGRANDE',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'SABANALARGA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'MALAMBO',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'nombre' => 'CANDELARIA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'nombre' => 'PONEDERA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'nombre' => 'POLONUEVO',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'nombre' => 'CAMPO DE LA CRUZ',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'nombre' => 'SANTO TOMAS',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'nombre' => 'MANATI',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'nombre' => 'REPELON',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'nombre' => 'LURUACO',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
            'nombre' => 'BARRANQUILLA (DISTRITO)',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'nombre' => 'PIOJO',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'nombre' => 'PALMAR DE VARELA',
                'id_departamento' => 8,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'nombre' => 'BARRANQUILLA D.E.',
                'id_departamento' => 9,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'nombre' => 'SANTAFE DE BOGOTA D.C.',
                'id_departamento' => 11,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'nombre' => 'ARJONA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'nombre' => 'EL CARMEN DE BOLIVAR',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'nombre' => 'ACHI',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'nombre' => 'ARENAL',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'nombre' => 'SAN MARTIN DE LOBA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'nombre' => 'SAN FERNANDO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'nombre' => 'MAGANGUE',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'nombre' => 'TIQUISIO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'nombre' => 'SAN JUAN NEPOMUCENO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'nombre' => 'MORALES',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'nombre' => 'ZAMBRANO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'nombre' => 'TALAIGUA NUEVO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'nombre' => 'EL PE?ON',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'nombre' => 'SIMITI',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'nombre' => 'CALAMAR',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'nombre' => 'BARRANCO DE LOBA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'nombre' => 'MAHATES',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'nombre' => 'ALTOS DEL ROSARIO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'nombre' => 'MOMPOS',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'nombre' => 'SAN JACINTO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'nombre' => 'SANTA ROSA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'nombre' => 'CICUCO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'nombre' => 'RIO VIEJO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'nombre' => 'MARGARITA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'nombre' => 'PINILLOS',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'nombre' => 'CLEMENCIA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'nombre' => 'CANTAGALLO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'nombre' => 'TURBACO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'nombre' => 'VILLANUEVA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'nombre' => 'SANTA ROSA DEL SUR',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'nombre' => 'SOPLAVIENTO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'nombre' => 'TURBANA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'nombre' => 'HATILLO DE LOBA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'nombre' => 'SANTA CATALINA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'nombre' => 'SAN ESTANISLAO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
            'nombre' => 'CARTAGENA (DISTRITO)',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'nombre' => 'MARIA LA BAJA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'nombre' => 'CORDOBA',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'nombre' => 'SAN PABLO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'nombre' => 'SAN CRISTOBAL',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'nombre' => 'MONTECRISTO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'nombre' => 'EL GUAMO',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'nombre' => 'REGIDOR',
                'id_departamento' => 13,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'nombre' => 'SACHICA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'nombre' => 'SOATA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'nombre' => 'SAMACA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'nombre' => 'PAYA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'nombre' => 'SOGAMOSO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'nombre' => 'PACHAVITA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'nombre' => 'SOCOTA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'nombre' => 'SOMONDOCO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'nombre' => 'AQUITANIA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'nombre' => 'SOCHA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'nombre' => 'SABOYA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'nombre' => 'LA VICTORIA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'nombre' => 'IZA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'nombre' => 'LA UVITA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'nombre' => 'SANTA SOFIA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'nombre' => 'JENESANO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'nombre' => 'JERICO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'nombre' => 'SAN MIGUEL DE SEMA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'nombre' => 'SIACHOQUE',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'nombre' => 'RAQUIRA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'nombre' => 'SAN LUIS DE GACENO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'nombre' => 'SANTA MARIA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'nombre' => 'SAN PABLO DE BORBUR',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'nombre' => 'LABRANZAGRANDE',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'nombre' => 'MIRAFLORES',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'nombre' => 'SORA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'nombre' => 'SANTANA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'nombre' => 'SANTA ROSA DE VITERBO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'nombre' => 'MONGUI',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'nombre' => 'MARIPI',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'nombre' => 'RONDON',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'nombre' => 'SAN EDUARDO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'nombre' => 'MONIQUIRA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'nombre' => 'GUATEQUE',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'nombre' => 'GUAYATA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'nombre' => 'SAN MATEO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'nombre' => 'SAN JOSE DE PARE',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'nombre' => 'VILLA DE LEYVA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'nombre' => 'GUICAN',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'nombre' => 'MACANAL',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'nombre' => 'SATIVASUR',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'nombre' => 'SATIVANORTE',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'nombre' => 'MONGUA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'nombre' => 'RAMIRIQUI',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'nombre' => 'GUACAMAYAS',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'nombre' => 'LA CAPILLA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'nombre' => 'ALMEIDA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'nombre' => 'CHIVOR',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'nombre' => 'CHIVATA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'nombre' => 'CIENEGA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'nombre' => 'ARCABUCO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'nombre' => 'FLORESTA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'nombre' => 'BELEN',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'nombre' => 'SOTAQUIRA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'nombre' => 'VENTAQUEMADA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'nombre' => 'COPER',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'nombre' => 'OTANCHE',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'nombre' => 'CHITA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'nombre' => 'COVARACHIA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'nombre' => 'VIRACACHA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'nombre' => 'UMBITA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'nombre' => 'CUBARA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'nombre' => 'CUCAITA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'nombre' => 'TUTASA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'nombre' => 'OICATA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'nombre' => 'TUTA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'nombre' => 'TURMEQUE',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'nombre' => 'CUITIVA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'nombre' => 'TUNUNGUA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'nombre' => 'CHIQUIZA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'nombre' => 'CORRALES',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'nombre' => 'BETEITIVA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'nombre' => 'TUNJA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'nombre' => 'PAZ DE RIO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'nombre' => 'BOYACA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'nombre' => 'BRICENO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'nombre' => 'BUENAVISTA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'nombre' => 'BUSBANZA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'nombre' => 'PAUNA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'nombre' => 'PESCA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'nombre' => 'PANQUEBA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'nombre' => 'CALDAS',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'nombre' => 'CHITARAQUE',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'nombre' => 'BOAVITA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'nombre' => 'COMBITA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'nombre' => 'PAJARITO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'nombre' => 'BERBEO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'nombre' => 'ZETAQUIRA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'nombre' => 'CERINZA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'nombre' => 'CHINAVITA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'nombre' => 'PISVA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'nombre' => 'CHIQUINQUIRA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'nombre' => 'PAIPA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'nombre' => 'PAEZ',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'nombre' => 'CHISCAS',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'nombre' => 'CAMPOHERMOSO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'nombre' => 'TIBANA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'nombre' => 'GARAGOA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'nombre' => 'TINJACA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'nombre' => 'EL ESPINO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'nombre' => 'NOBSA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'nombre' => 'TOTA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'nombre' => 'TIPACOQUE',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'nombre' => 'TASCO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'nombre' => 'GAMEZA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'nombre' => 'PUERTO BOYACA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'nombre' => 'SUSACON',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'nombre' => 'FIRAVITOBA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'nombre' => 'QUIPAMA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'nombre' => 'SUTAMARCHAN',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'nombre' => 'TENZA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'nombre' => 'TIBASOSA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'nombre' => 'GACHANTIVA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'nombre' => 'SUTATENZA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'nombre' => 'MOTAVITA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'nombre' => 'TOPAGA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'nombre' => 'SORACA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'nombre' => 'TOCA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'nombre' => 'DUITAMA',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'nombre' => 'EL COCUY',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'nombre' => 'NUEVO COLON',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'nombre' => 'MUZO',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'nombre' => 'TOGUI',
                'id_departamento' => 15,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'nombre' => 'FILADELFIA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'nombre' => 'MARMATO',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'nombre' => 'VITERBO',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'nombre' => 'PACORA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'nombre' => 'MANZANARES',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'nombre' => 'RISARALDA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'nombre' => 'ARANZAZU',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'nombre' => 'AGUADAS',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'nombre' => 'MARULANDA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'nombre' => 'RIOSUCIO',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'nombre' => 'NEIRA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'nombre' => 'CHINCHINA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'nombre' => 'VILLAMARIA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'nombre' => 'ANSERMA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'nombre' => 'VICTORIA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'nombre' => 'BELALCAZAR',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'nombre' => 'SUPIA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'nombre' => 'LA MERCED',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'nombre' => 'SALAMINA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'nombre' => 'SAMANA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'nombre' => 'PALESTINA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'nombre' => 'MARQUETALIA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
                'nombre' => 'MANIZALES',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'nombre' => 'PENSILVANIA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'nombre' => 'LA DORADA',
                'id_departamento' => 17,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'nombre' => 'SAN JOSE DE FRAGUA',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'nombre' => 'EL DONCELLO',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'nombre' => 'SOLITA',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'nombre' => 'PUERTO RICO',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
                'nombre' => 'SOLANO',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'nombre' => 'ALBANIA',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'nombre' => 'CARTAGENA DEL CHAIRA',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'nombre' => 'MILAN',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'nombre' => 'LA MONTA?ITA',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'nombre' => 'MORELIA',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'nombre' => 'FLORENCIA',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'nombre' => 'SAN VICENTE DEL CAGUAN',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'nombre' => 'BELEN DE LOS ANDAQUIES',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'nombre' => 'CURILLO',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'nombre' => 'EL PAUJIL',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
                'nombre' => 'VALPARAISO',
                'id_departamento' => 18,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
            'nombre' => 'PATIA  (EL BORDO)',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'nombre' => 'MORALES',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'nombre' => 'CALDONO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'nombre' => 'POPAYAN',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'nombre' => 'BOLIVAR',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
            'nombre' => 'SOTARA  (PAISPAMBA )',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'nombre' => 'BUENOS AIRES',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
                'nombre' => 'ROSAS',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'nombre' => 'CAJIBIO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
            'nombre' => 'PURACE ( COCONUCO )',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => 243,
                'nombre' => 'ARGELIA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => 244,
                'nombre' => 'PUERTO TEJADA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => 245,
                'nombre' => 'LA SIERRA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => 246,
                'nombre' => 'MIRANDA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'nombre' => 'CORINTO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'nombre' => 'TIMBIO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'nombre' => 'ALMAGUER',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => 250,
                'nombre' => 'LA VEGA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => 251,
                'nombre' => 'SAN SEBASTIAN',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => 252,
                'nombre' => 'SILVIA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => 253,
                'nombre' => 'INZA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => 254,
                'nombre' => 'SANTANDER DE QUILICHAO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => 255,
                'nombre' => 'SANTA ROSA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => 256,
                'nombre' => 'JAMBALO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => 257,
                'nombre' => 'TOTORO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => 258,
                'nombre' => 'TORIBIO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => 259,
                'nombre' => 'TIMBIQUI',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => 260,
                'nombre' => 'LOPEZ',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => 261,
                'nombre' => 'MERCADERES',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => 262,
                'nombre' => 'CALOTO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => 263,
                'nombre' => 'GUAPI',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => 264,
                'nombre' => 'PADILLA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => 265,
                'nombre' => 'BALBOA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => 266,
            'nombre' => 'PAEZ  (BELALCAZAR )',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => 267,
                'nombre' => 'VILLA RICA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => 268,
                'nombre' => 'PIENDAMO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => 269,
                'nombre' => 'EL TAMBO',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => 270,
                'nombre' => 'SUAREZ',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => 271,
                'nombre' => 'FLORENCIA',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => 272,
                'nombre' => 'PAILITAS',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => 273,
                'nombre' => 'LA PAZ',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => 274,
            'nombre' => 'MANAURE  (BALC#N DEL CESAR)',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => 275,
                'nombre' => 'RIO DE ORO',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => 276,
                'nombre' => 'PELAYA',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => 277,
                'nombre' => 'GAMARRA',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => 278,
                'nombre' => 'SAN ALBERTO',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => 279,
                'nombre' => 'CURUMANI',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => 280,
                'nombre' => 'LA GLORIA',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => 281,
                'nombre' => 'SAN DIEGO',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => 282,
                'nombre' => 'EL PASO',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => 283,
                'nombre' => 'TAMALAMEQUE',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => 284,
                'nombre' => 'CHIRIGUANA',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => 285,
                'nombre' => 'EL COPEY',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => 286,
                'nombre' => 'CHIMICHAGUA',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => 287,
                'nombre' => 'SAN MARTIN',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => 288,
                'nombre' => 'AGUACHICA',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => 289,
                'nombre' => 'AGUSTIN CODAZZI',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => 290,
                'nombre' => 'ASTREA',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => 291,
                'nombre' => 'BECERRIL',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => 292,
                'nombre' => 'BOSCONIA',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => 293,
                'nombre' => 'VALLEDUPAR',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => 294,
                'nombre' => 'GONZALEZ',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => 295,
                'nombre' => 'LA JAGUA DE IBIRICO',
                'id_departamento' => 20,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => 296,
                'nombre' => 'CIENAGA DE ORO',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => 297,
                'nombre' => 'PURISIMA',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => 298,
                'nombre' => 'PUERTO LIBERTADOR',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => 299,
                'nombre' => 'BUENAVISTA',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => 300,
                'nombre' => 'TIERRALTA',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => 301,
                'nombre' => 'PUEBLO NUEVO',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => 302,
                'nombre' => 'VALENCIA',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => 303,
                'nombre' => 'PUERTO ESCONDIDO',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => 304,
                'nombre' => 'CHINU',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => 305,
                'nombre' => 'PLANETA RICA',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => 306,
                'nombre' => 'CHIMA',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => 307,
                'nombre' => 'CERETE',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => 308,
                'nombre' => 'CANALETE',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => 309,
                'nombre' => 'MONTERIA',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => 310,
                'nombre' => 'MO?ITOS',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => 311,
                'nombre' => 'AYAPEL',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => 312,
                'nombre' => 'SAN BERNARDO DEL VIENTO',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => 313,
                'nombre' => 'SAN CARLOS',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => 314,
                'nombre' => 'MOMIL',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => 315,
                'nombre' => 'SAN ANTERO',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => 316,
                'nombre' => 'LORICA',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => 317,
                'nombre' => 'SAN ANDRES SOTAVENTO',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => 318,
                'nombre' => 'LOS CORDOBAS',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => 319,
                'nombre' => 'MONTELIBANO',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => 320,
                'nombre' => 'SAN PELAYO',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => 321,
                'nombre' => 'SAHAGUN',
                'id_departamento' => 23,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => 322,
                'nombre' => 'CHOCONTA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => 323,
                'nombre' => 'PACHO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => 324,
                'nombre' => 'PAIME',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => 325,
                'nombre' => 'LA VEGA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => 326,
                'nombre' => 'AGUA DE DIOS',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => 327,
                'nombre' => 'CHOACHI',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => 328,
                'nombre' => 'LA PE?A',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => 329,
                'nombre' => 'ALBAN',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => 330,
                'nombre' => 'CHIPAQUE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => 331,
                'nombre' => 'COGUA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => 332,
                'nombre' => 'CHIA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => 333,
                'nombre' => 'ANOLAIMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => 334,
                'nombre' => 'MADRID',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => 335,
                'nombre' => 'MANTA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => 336,
                'nombre' => 'CABRERA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => 337,
                'nombre' => 'PARATEBUENO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => 338,
                'nombre' => 'ARBELAEZ',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => 339,
                'nombre' => 'PANDI',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => 340,
                'nombre' => 'CARMEN DE CARUPA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => 341,
                'nombre' => 'CAJICA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => 342,
                'nombre' => 'CHAGUANI',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => 343,
                'nombre' => 'MACHETA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => 344,
                'nombre' => 'CAPARRAPI',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => 345,
                'nombre' => 'CUCUNUBA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => 346,
                'nombre' => 'CAQUEZA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => 347,
                'nombre' => 'LA PALMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => 348,
                'nombre' => 'LENGUAZAQUE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => 349,
                'nombre' => 'CACHIPAY',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => 350,
                'nombre' => 'GACHETA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => 351,
                'nombre' => 'VENECIA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => 352,
                'nombre' => 'FUNZA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => 353,
                'nombre' => 'FUQUENE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => 354,
                'nombre' => 'FUSAGASUGA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => 355,
                'nombre' => 'GUAYABETAL',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => 356,
                'nombre' => 'GACHALA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => 357,
                'nombre' => 'MEDINA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => 358,
                'nombre' => 'GUAYABAL DE SIQUIMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => 359,
                'nombre' => 'GUTIERREZ',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => 360,
                'nombre' => 'GAMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => 361,
                'nombre' => 'MOSQUERA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => 362,
                'nombre' => 'GUATAVITA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => 363,
                'nombre' => 'GIRARDOT',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => 364,
                'nombre' => 'GUATAQUI',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => 365,
                'nombre' => 'GUASCA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => 366,
                'nombre' => 'GUACHETA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => 367,
                'nombre' => 'GACHANCIPA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => 368,
                'nombre' => 'ANAPOIMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => 369,
                'nombre' => 'COTA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => 370,
                'nombre' => 'GUADUAS',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => 371,
                'nombre' => 'LA MESA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => 372,
                'nombre' => 'LA CALERA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => 373,
                'nombre' => 'JUNIN',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => 374,
                'nombre' => 'EL COLEGIO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => 375,
                'nombre' => 'FOSCA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => 376,
                'nombre' => 'JERUSALEN',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => 377,
                'nombre' => 'NARI?O',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => 378,
                'nombre' => 'EL PE?ON',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => 379,
                'nombre' => 'NIMAIMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => 380,
                'nombre' => 'NILO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => 381,
                'nombre' => 'FACATATIVA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => 382,
                'nombre' => 'NEMOCON',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => 383,
                'nombre' => 'FOMEQUE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => 384,
                'nombre' => 'GRANADA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => 385,
                'nombre' => 'NOCAIMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => 386,
                'nombre' => 'BELTRAN',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => 387,
                'nombre' => 'VILLAPINZON',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => 388,
                'nombre' => 'VILLAGOMEZ',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => 389,
                'nombre' => 'VIANI',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => 390,
                'nombre' => 'PUERTO SALGAR',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => 391,
                'nombre' => 'SASAIMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => 392,
                'nombre' => 'UBAQUE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => 393,
                'nombre' => 'SESQUILE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => 394,
                'nombre' => 'PULI',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => 395,
                'nombre' => 'SIBATE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => 396,
                'nombre' => 'UTICA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => 397,
                'nombre' => 'SILVANIA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => 398,
                'nombre' => 'UNE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => 399,
                'nombre' => 'UBATE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => 400,
                'nombre' => 'SIMIJACA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => 401,
                'nombre' => 'VIOTA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => 402,
                'nombre' => 'SAN JUAN DE RIO SECO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => 403,
                'nombre' => 'RICAURTE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => 404,
                'nombre' => 'S.ANTONIO TEQUENDAMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => 405,
                'nombre' => 'SAN BERNARDO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => 406,
                'nombre' => 'SAN CAYETANO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => 407,
                'nombre' => 'SAN FRANCISCO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => 408,
                'nombre' => 'BOJACA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => 409,
                'nombre' => 'VILLETA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => 410,
                'nombre' => 'QUIPILE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => 411,
                'nombre' => 'YACOPI',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => 412,
                'nombre' => 'QUETAME',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => 413,
                'nombre' => 'QUEBRADANEGRA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => 414,
                'nombre' => 'BITUIMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => 415,
                'nombre' => 'ZIPAQUIRA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => 416,
                'nombre' => 'ZIPACON',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => 417,
                'nombre' => 'VERGARA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => 418,
                'nombre' => 'APULO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => 419,
                'nombre' => 'TIBACUY',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => 420,
                'nombre' => 'SUSA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => 421,
                'nombre' => 'SUPATA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => 422,
                'nombre' => 'TABIO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => 423,
                'nombre' => 'TOCAIMA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => 424,
                'nombre' => 'TAUSA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => 425,
                'nombre' => 'TIBIRITA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => 426,
                'nombre' => 'SOACHA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => 427,
                'nombre' => 'TOCANCIPA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => 428,
                'nombre' => 'SUTATAUSA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => 429,
                'nombre' => 'SUESCA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => 430,
                'nombre' => 'SUBACHOQUE',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => 431,
                'nombre' => 'TOPAIPI',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => 432,
                'nombre' => 'TENJO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => 433,
                'nombre' => 'UBALA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => 434,
                'nombre' => 'PASCA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => 435,
                'nombre' => 'SOPO',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => 436,
                'nombre' => 'TENA',
                'id_departamento' => 25,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => 437,
                'nombre' => 'QUIBDO',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => 438,
                'nombre' => 'RIOSUCIO',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => 439,
                'nombre' => 'TADO',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => 440,
                'nombre' => 'CANTON DEL SAN PABLO',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => 441,
            'nombre' => 'BOJAYA ( BELLAVISTA )',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => 442,
            'nombre' => 'ALTO BAUDO ( PIE DE PATO )',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => 443,
                'nombre' => 'SAN JOSE DEL PALMAR',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => 444,
                'nombre' => 'UNGUIA',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => 445,
                'nombre' => 'ACANDI',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => 446,
                'nombre' => 'LLORO',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => 447,
            'nombre' => 'BAJO BAUDO ( PIZARRO )',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => 448,
                'nombre' => 'BAGADO',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => 449,
            'nombre' => 'LITORAL DE SAN JUAN (DOCORDO)',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => 450,
                'nombre' => 'CONDOTO',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => 451,
                'nombre' => 'NOVITA',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => 452,
                'nombre' => 'NUQUI',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => 453,
                'nombre' => 'ITSMINA',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => 454,
                'nombre' => 'SIPI',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => 455,
                'nombre' => 'EL CARMEN',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => 456,
                'nombre' => 'JURADO',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => 457,
            'nombre' => 'BAHIA SOLANO ( MUTIS )',
                'id_departamento' => 27,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => 458,
                'nombre' => 'GUADALUPE',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => 459,
                'nombre' => 'GIGANTE',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => 460,
                'nombre' => 'CAMPOALEGRE',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => 461,
                'nombre' => 'ELIAS',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => 462,
                'nombre' => 'TELLO',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => 463,
                'nombre' => 'TERUEL',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => 464,
                'nombre' => 'TESALIA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => 465,
                'nombre' => 'SALADOBLANCO',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => 466,
                'nombre' => 'SUAZA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => 467,
                'nombre' => 'ACEVEDO',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => 468,
                'nombre' => 'LA PLATA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => 469,
                'nombre' => 'COLOMBIA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => 470,
                'nombre' => 'VILLAVIEJA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => 471,
                'nombre' => 'TIMANA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => 472,
                'nombre' => 'PALO CABILDO',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => 473,
                'nombre' => 'TARQUI',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => 474,
                'nombre' => 'IQUIRA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => 475,
                'nombre' => 'SANTA MARIA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => 476,
                'nombre' => 'ISNOS',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => 477,
                'nombre' => 'YAGUARA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => 478,
                'nombre' => 'GARZON',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => 479,
                'nombre' => 'HOBO',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => 480,
                'nombre' => 'LA ARGENTINA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => 481,
                'nombre' => 'SAN AGUSTIN',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => 482,
                'nombre' => 'RIVERA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => 483,
                'nombre' => 'ALTAMIRA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => 484,
                'nombre' => 'PALERMO',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => 485,
                'nombre' => 'NATAGA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => 486,
                'nombre' => 'PALESTINA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => 487,
                'nombre' => 'AIPE',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => 488,
                'nombre' => 'PITAL',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => 489,
                'nombre' => 'PITALITO',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => 490,
                'nombre' => 'ALGECIRAS',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => 491,
                'nombre' => 'OPORAPA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => 492,
                'nombre' => 'BARAYA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => 493,
                'nombre' => 'NEIVA',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => 494,
                'nombre' => 'AGRADO',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => 495,
                'nombre' => 'PAICOL',
                'id_departamento' => 41,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            495 => 
            array (
                'id' => 496,
                'nombre' => 'HATO NUEVO',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            496 => 
            array (
                'id' => 497,
                'nombre' => 'FONSECA',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            497 => 
            array (
                'id' => 498,
                'nombre' => 'DISTRACCION',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            498 => 
            array (
                'id' => 499,
                'nombre' => 'DIBULLA',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            499 => 
            array (
                'id' => 500,
                'nombre' => 'URUMITA',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        \DB::table('tbl_municipios')->insert(array (
            0 => 
            array (
                'id' => 501,
                'nombre' => 'VILLANUEVA',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 502,
                'nombre' => 'RIOHACHA',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 503,
                'nombre' => 'URIBIA',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 504,
                'nombre' => 'MANAURE',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 505,
                'nombre' => 'BARRANCAS',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 506,
                'nombre' => 'PITI?O DEL CARMEN',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 507,
                'nombre' => 'MAICAO',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 508,
                'nombre' => 'SAN JUAN DEL CESAR',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 509,
                'nombre' => 'EL MOLINO',
                'id_departamento' => 44,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 510,
                'nombre' => 'SANTA MARTA D.E.',
                'id_departamento' => 48,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 511,
                'nombre' => 'TUMACO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 512,
                'nombre' => 'CONSACA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 513,
            'nombre' => 'ARBOLEDA ( BERRUECOS )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 514,
                'nombre' => 'GUACHUCAL',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 515,
                'nombre' => 'POLICARPA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 516,
                'nombre' => 'TUQUERRES',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 517,
                'nombre' => 'EL CHARCO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 518,
                'nombre' => 'IMUES',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 519,
                'nombre' => 'CONTADERO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 520,
                'nombre' => 'EL ROSARIO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 521,
                'nombre' => 'IPIALES',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 522,
                'nombre' => 'POTOSI',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 523,
            'nombre' => 'COLON ( G?NOVA )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 524,
                'nombre' => 'EL TAMBO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 525,
                'nombre' => 'EL TABLON',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 526,
                'nombre' => 'ILES',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 527,
                'nombre' => 'FUNES',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 528,
                'nombre' => 'CHACHAGUI',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 529,
                'nombre' => 'GUAITARILLA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 530,
                'nombre' => 'PROVIDENCIA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 531,
                'nombre' => 'CORDOBA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 532,
                'nombre' => 'GUALMATAN',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 533,
                'nombre' => 'BELEN',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 534,
                'nombre' => 'PASTO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 535,
                'nombre' => 'OSPINA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 536,
                'nombre' => 'BARBACOAS',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 537,
            'nombre' => 'CUASPUD ( CARLOSAMA )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 538,
            'nombre' => 'OLAYA HERRERA ( BOCAS DE SATINGA )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 539,
                'nombre' => 'CUMBAL',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 540,
                'nombre' => 'SAPUYES',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 541,
                'nombre' => 'TAMINANGO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 542,
                'nombre' => 'CUMBITARA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 543,
                'nombre' => 'TANGUA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 544,
            'nombre' => 'FRANCISCO PIZARRO ( SALAHONDA )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 545,
                'nombre' => 'SANDONA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 546,
                'nombre' => 'ALDANA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 547,
                'nombre' => 'SAN LORENZO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 548,
                'nombre' => 'LA UNION',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 549,
                'nombre' => 'LINARES',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 550,
                'nombre' => 'LA FLORIDA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 551,
                'nombre' => 'SAN BERNARDO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 552,
            'nombre' => 'MAGUI ( PAY#N )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 553,
            'nombre' => 'ALBAN ( SAN JOS? )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 554,
                'nombre' => 'LA CRUZ',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 555,
                'nombre' => 'LA TOLA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 556,
                'nombre' => 'LEIVA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 557,
                'nombre' => 'BUESACO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 558,
                'nombre' => 'PUPIALES',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 559,
                'nombre' => 'SAMANIEGO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 560,
                'nombre' => 'YACUANQUER',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 561,
                'nombre' => 'LA LLANADA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 562,
            'nombre' => 'SANTACRUZ ( GUACHAVES )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 563,
                'nombre' => 'SAN PABLO',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 564,
                'nombre' => 'PUERRES',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 565,
                'nombre' => 'EL PE?OL',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 566,
            'nombre' => 'SAN PEDRO DE CARTAGO ( CARTAGO )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 567,
            'nombre' => 'LOS ANDES ( SOTOMAYOR )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 568,
                'nombre' => 'EL PE?OL',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 569,
            'nombre' => 'MALLAMA ( PIEDRANCHA )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 570,
                'nombre' => 'MOSQUERA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 571,
                'nombre' => 'ANCUYA',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 572,
            'nombre' => 'ROBERTO PAYAN ( SAN JOS? )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 573,
            'nombre' => 'SANTA BARBARA ( ISCUANDE )',
                'id_departamento' => 52,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 574,
                'nombre' => 'PUERTO LLERAS',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 575,
                'nombre' => 'SAN CARLOS GUAROA',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 576,
                'nombre' => 'CUBARRAL',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 577,
                'nombre' => 'VILLAVICENCIO',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 578,
                'nombre' => 'SAN MARTIN',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 579,
                'nombre' => 'BARRANCA DE UPIA',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 580,
                'nombre' => 'LA URIBE',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 581,
                'nombre' => 'PUERTO LOPEZ',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 582,
                'nombre' => 'LA MACARENA',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 583,
                'nombre' => 'PUERTO GAITAN',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 584,
                'nombre' => 'SAN JUANITO',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 585,
                'nombre' => 'MESETAS',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 586,
                'nombre' => 'MAPIRIPAN',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 587,
                'nombre' => 'SAN JUAN DE ARAMA',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 588,
                'nombre' => 'FUENTE DE ORO',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 589,
                'nombre' => 'PUERTO CONCORDIA',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 590,
                'nombre' => 'RESTREPO',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 591,
                'nombre' => 'EL CASTILLO',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 592,
                'nombre' => 'ACACIAS',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 593,
                'nombre' => 'EL DORADO',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 594,
                'nombre' => 'CABUYARO',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 595,
                'nombre' => 'CASTILLA LA NUEVA',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 596,
                'nombre' => 'PUERTO RICO',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 597,
                'nombre' => 'EL CALVARIO',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 598,
                'nombre' => 'CUMARAL',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 599,
                'nombre' => 'VISTA HERMOSA',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 600,
                'nombre' => 'LEJANIAS',
                'id_departamento' => 50,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 601,
                'nombre' => 'CHINACOTA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 602,
                'nombre' => 'EL ZULIA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 603,
                'nombre' => 'GRAMALOTE',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 604,
                'nombre' => 'ARBOLEDAS',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 605,
                'nombre' => 'LOS PATIOS',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 606,
                'nombre' => 'ABREGO',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 607,
                'nombre' => 'CUCUTILLA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 608,
                'nombre' => 'DURANIA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 609,
                'nombre' => 'RAGONVALIA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 610,
                'nombre' => 'BOCHALEMA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 611,
                'nombre' => 'CUCUTA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 612,
                'nombre' => 'TIBU',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 613,
                'nombre' => 'PAMPLONITA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 614,
                'nombre' => 'CACOTA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 615,
                'nombre' => 'EL TARRA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 616,
                'nombre' => 'CHITAGA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 617,
                'nombre' => 'PUERTO SANTANDER',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 618,
                'nombre' => 'CACHIRA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 619,
                'nombre' => 'SAN CALIXTO',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 620,
                'nombre' => 'PAMPLONA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 621,
                'nombre' => 'SAN CAYETANO',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 622,
                'nombre' => 'TOLEDO',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 623,
                'nombre' => 'SALAZAR',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 624,
                'nombre' => 'LOURDES',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 625,
                'nombre' => 'SARDINATA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 626,
                'nombre' => 'CONVENCION',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 627,
                'nombre' => 'SILOS',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 628,
                'nombre' => 'VILLA CARO',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 629,
                'nombre' => 'LABATECA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 630,
                'nombre' => 'TEORAMA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 631,
                'nombre' => 'LA ESPERANZA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 632,
                'nombre' => 'HERRAN',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 633,
                'nombre' => 'OCA?A',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 634,
                'nombre' => 'HACARI',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 635,
                'nombre' => 'VILLA DEL ROSARIO',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => 636,
                'nombre' => 'BUCARASICA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 637,
                'nombre' => 'MUTISCUA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 638,
                'nombre' => 'SANTIAGO',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 639,
                'nombre' => 'LA PLAYA',
                'id_departamento' => 54,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 640,
                'nombre' => 'FILANDIA',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 641,
                'nombre' => 'MONTENEGRO',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 642,
                'nombre' => 'SALENTO',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 643,
                'nombre' => 'PIJAO',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 644,
                'nombre' => 'CIRCASIA',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 645,
                'nombre' => 'VEREDA MURILLO',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 646,
                'nombre' => 'BUENAVISTA',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 647,
                'nombre' => 'QUIMBAYA',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => 648,
                'nombre' => 'LA TEBAIDA',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => 649,
                'nombre' => 'CALARCA',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => 650,
                'nombre' => 'ARMENIA',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => 651,
                'nombre' => 'GENOVA',
                'id_departamento' => 63,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => 652,
                'nombre' => 'PEREIRA',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => 653,
                'nombre' => 'SANTUARIO',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => 654,
                'nombre' => 'QUINCHIA',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => 655,
                'nombre' => 'MARSELLA',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => 656,
                'nombre' => 'LA VIRGINIA',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => 657,
                'nombre' => 'GUATICA',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => 658,
                'nombre' => 'MISTRATO',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => 659,
                'nombre' => 'BELEN DE UMBRIA',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => 660,
                'nombre' => 'PUEBLO RICO',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => 661,
                'nombre' => 'APIA',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => 662,
                'nombre' => 'DOS QUEBRADAS',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => 663,
                'nombre' => 'BALBOA',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => 664,
                'nombre' => 'LA CELIA',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => 665,
                'nombre' => 'SANTA ROSA DE CABAL',
                'id_departamento' => 66,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => 666,
                'nombre' => 'CALIFORNIA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => 667,
                'nombre' => 'VETAS',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => 668,
                'nombre' => 'EL PLAYON',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => 669,
                'nombre' => 'CONCEPCION',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => 670,
                'nombre' => 'CHIPATA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => 671,
                'nombre' => 'EL PE?ON',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => 672,
                'nombre' => 'CIMITARRA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => 673,
                'nombre' => 'BUCARAMANGA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => 674,
                'nombre' => 'VILLANUEVA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => 675,
                'nombre' => 'CEPITA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => 676,
                'nombre' => 'ARATOCA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => 677,
                'nombre' => 'ALBANIA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => 678,
                'nombre' => 'AGUADA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => 679,
                'nombre' => 'BARBOSA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => 680,
                'nombre' => 'BARICHARA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => 681,
                'nombre' => 'EL CARMEN',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => 682,
                'nombre' => 'COROMORO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => 683,
                'nombre' => 'TONA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => 684,
                'nombre' => 'CHARTA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => 685,
                'nombre' => 'VALLE DE SAN JOSE',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => 686,
                'nombre' => 'CONTRATACION',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => 687,
                'nombre' => 'CERRITO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => 688,
                'nombre' => 'BETULIA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => 689,
                'nombre' => 'ZAPATOCA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => 690,
                'nombre' => 'CABRERA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => 691,
                'nombre' => 'CARCASI',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => 692,
                'nombre' => 'CHIMA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => 693,
                'nombre' => 'VELEZ',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => 694,
                'nombre' => 'CURITI',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => 695,
                'nombre' => 'BARRANCABERMEJA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => 696,
                'nombre' => 'CONFINES',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => 697,
                'nombre' => 'CAPITANEJO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => 698,
                'nombre' => 'EL GUACAMAYO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => 699,
                'nombre' => 'CHARALA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => 700,
                'nombre' => 'HATO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => 701,
                'nombre' => 'LA PAZ',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => 702,
                'nombre' => 'LANDAZURI',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => 703,
                'nombre' => 'SAN MIGUEL',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => 704,
                'nombre' => 'LA BELLEZA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => 705,
                'nombre' => 'SAN VICENTE DE CHUCURI',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => 706,
                'nombre' => 'JORDAN',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => 707,
                'nombre' => 'JESUS MARIA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => 708,
                'nombre' => 'GIRON',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => 709,
                'nombre' => 'PALMAS DEL SOCORRO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => 710,
                'nombre' => 'LEBRIJA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => 711,
                'nombre' => 'SANTA HELENA DEL OPON',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => 712,
                'nombre' => 'GUEPSA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => 713,
                'nombre' => 'GUAVATA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => 714,
                'nombre' => 'GUAPOTA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => 715,
                'nombre' => 'SIMACOTA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => 716,
                'nombre' => 'GUADALUPE',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => 717,
                'nombre' => 'GUACA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => 718,
                'nombre' => 'SOCORRO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => 719,
                'nombre' => 'SANTA BARBARA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => 720,
                'nombre' => 'SABANA DE TORRES',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => 721,
                'nombre' => 'ONZAGA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => 722,
                'nombre' => 'OIBA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => 723,
                'nombre' => 'OCAMONTE',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => 724,
                'nombre' => 'PUENTE NACIONAL',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => 725,
                'nombre' => 'PINCHOTE',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => 726,
                'nombre' => 'PUERTO PARRA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => 727,
                'nombre' => 'PUERTO WILCHES',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => 728,
                'nombre' => 'MOLAGAVITA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => 729,
                'nombre' => 'SAN JOAQUIN',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => 730,
                'nombre' => 'MATANZA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => 731,
                'nombre' => 'SAN GIL',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => 732,
                'nombre' => 'MALAGA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => 733,
                'nombre' => 'MACARAVITA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => 734,
                'nombre' => 'PALMAR',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => 735,
                'nombre' => 'PIEDECUESTA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 736,
                'nombre' => 'SAN ANDRES',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => 737,
                'nombre' => 'LOS SANTOS',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => 738,
                'nombre' => 'SAN BENITO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => 739,
                'nombre' => 'SAN JOSE DE MIRANDA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => 740,
                'nombre' => 'MOGOTES',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => 741,
                'nombre' => 'SUCRE',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 742,
                'nombre' => 'FLORIAN',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => 743,
                'nombre' => 'GAMBITA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => 744,
                'nombre' => 'PARAMO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => 745,
                'nombre' => 'SURATA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => 746,
                'nombre' => 'FLORIDABLANCA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 747,
                'nombre' => 'GALAN',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 748,
                'nombre' => 'ENCISO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 749,
                'nombre' => 'ENCINO',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => 750,
                'nombre' => 'SUAITA',
                'id_departamento' => 68,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => 751,
                'nombre' => 'TOLU',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => 752,
                'nombre' => 'SINCE',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => 753,
                'nombre' => 'PALMITO',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => 754,
                'nombre' => 'TOLUVIEJO',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => 755,
                'nombre' => 'SAN PEDRO',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => 756,
                'nombre' => 'SAN ONOFRE',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => 757,
                'nombre' => 'SUCRE',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => 758,
                'nombre' => 'COROZAL',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => 759,
                'nombre' => 'SAN MARCOS',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => 760,
                'nombre' => 'SAN BENITO ABAD',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => 761,
                'nombre' => 'SAN JUAN DE BETULIA',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => 762,
                'nombre' => 'BUENAVISTA',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => 763,
                'nombre' => 'GUARANDA',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => 764,
                'nombre' => 'COLOSO',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => 765,
                'nombre' => 'MAJAGUAL',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => 766,
                'nombre' => 'CHALAN',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => 767,
                'nombre' => 'SINCELEJO',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => 768,
                'nombre' => 'OVEJAS',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => 769,
                'nombre' => 'CAIMITO',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => 770,
                'nombre' => 'GALERAS',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => 771,
                'nombre' => 'MORROA',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => 772,
                'nombre' => 'SAMPUES',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => 773,
                'nombre' => 'LOS PALMITOS',
                'id_departamento' => 70,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => 774,
                'nombre' => 'PIEDRAS',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => 775,
                'nombre' => 'IBAGUE',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => 776,
                'nombre' => 'MURILLO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => 777,
                'nombre' => 'MELGAR',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => 778,
                'nombre' => 'RIOBLANCO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => 779,
                'nombre' => 'MARIQUITA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => 780,
                'nombre' => 'CASABIANCA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => 781,
                'nombre' => 'RONCESVALLES',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => 782,
                'nombre' => 'SAN LUIS',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => 783,
                'nombre' => 'FALAN',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => 784,
                'nombre' => 'SAN ANTONIO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => 785,
                'nombre' => 'LIBANO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => 786,
                'nombre' => 'FRESNO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => 787,
                'nombre' => 'ROVIRA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => 788,
                'nombre' => 'CAJAMARCA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => 789,
                'nombre' => 'LERIDA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => 790,
                'nombre' => 'SALDA?A',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => 791,
                'nombre' => 'PLANADAS',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => 792,
                'nombre' => 'NATAGAIMA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => 793,
                'nombre' => 'ALPUJARRA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => 794,
                'nombre' => 'FLANDES',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => 795,
                'nombre' => 'CHAPARRAL',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => 796,
                'nombre' => 'ESPINAL',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => 797,
                'nombre' => 'CARMEN DE APICALA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => 798,
                'nombre' => 'PURIFICACION',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => 799,
                'nombre' => 'AMBALEMA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => 800,
                'nombre' => 'PALOCABILDO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => 801,
                'nombre' => 'ALVARADO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => 802,
                'nombre' => 'PRADO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => 803,
                'nombre' => 'VALLE DE SAN JUAN',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => 804,
                'nombre' => 'SUAREZ',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => 805,
                'nombre' => 'ICONONZO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => 806,
                'nombre' => 'VENADILLO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => 807,
                'nombre' => 'GUAMO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => 808,
                'nombre' => 'HONDA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => 809,
                'nombre' => 'DOLORES',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => 810,
                'nombre' => 'COYAIMA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => 811,
                'nombre' => 'HERVEO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => 812,
                'nombre' => 'COELLO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => 813,
                'nombre' => 'ORTEGA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => 814,
                'nombre' => 'CUNDAY',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => 815,
            'nombre' => 'ARMERO (GUAYABAL)',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => 816,
                'nombre' => 'ANZOATEGUI',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => 817,
                'nombre' => 'SANTA ISABEL',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => 818,
                'nombre' => 'ATACO',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => 819,
                'nombre' => 'VILLARRICA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => 820,
                'nombre' => 'VILLAHERMOSA',
                'id_departamento' => 73,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => 821,
                'nombre' => 'FLORIDA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => 822,
                'nombre' => 'CARTAGO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => 823,
                'nombre' => 'TORO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => 824,
                'nombre' => 'DAGUA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => 825,
                'nombre' => 'EL CERRITO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => 826,
                'nombre' => 'EL CAIRO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => 827,
                'nombre' => 'ULLOA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => 828,
                'nombre' => 'PALMIRA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => 829,
                'nombre' => 'EL DOVIO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => 830,
                'nombre' => 'GINEBRA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => 831,
                'nombre' => 'PRADERA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => 832,
                'nombre' => 'ROLDANILLO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => 833,
                'nombre' => 'RIOFRIO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => 834,
                'nombre' => 'CALI',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => 835,
                'nombre' => 'CANDELARIA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => 836,
                'nombre' => 'TRUJILLO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => 837,
                'nombre' => 'TULUA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => 838,
                'nombre' => 'GUACARI',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => 839,
                'nombre' => 'YOTOCO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => 840,
                'nombre' => 'ALCALA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => 841,
                'nombre' => 'VIJES',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => 842,
                'nombre' => 'JAMUNDI',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => 843,
                'nombre' => 'OBANDO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => 844,
                'nombre' => 'BUGALAGRANDE',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => 845,
                'nombre' => 'EL AGUILA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => 846,
                'nombre' => 'BUENAVENTURA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => 847,
                'nombre' => 'VERSALLES',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => 848,
                'nombre' => 'BUGA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => 849,
                'nombre' => 'LA CUMBRE',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => 850,
                'nombre' => 'ANDALUCIA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => 851,
                'nombre' => 'LA VICTORIA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => 852,
                'nombre' => 'YUMBO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => 853,
                'nombre' => 'ANSERMANUEVO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => 854,
                'nombre' => 'SEVILLA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => 855,
                'nombre' => 'CAICEDONIA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => 856,
                'nombre' => 'ZARZAL',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => 857,
                'nombre' => 'SAN PEDRO',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => 858,
                'nombre' => 'ARGELIA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => 859,
                'nombre' => 'DARIEN-CALIMA',
                'id_departamento' => 76,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => 860,
                'nombre' => 'FORTUL',
                'id_departamento' => 81,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => 861,
                'nombre' => 'ARAUCA',
                'id_departamento' => 81,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => 862,
                'nombre' => 'TAME',
                'id_departamento' => 81,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => 863,
                'nombre' => 'CRAVO NORTE',
                'id_departamento' => 81,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => 864,
                'nombre' => 'SARAVENA',
                'id_departamento' => 81,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => 865,
                'nombre' => 'PUERTO RONDON',
                'id_departamento' => 81,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => 866,
                'nombre' => 'ARAUQUITA',
                'id_departamento' => 81,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => 867,
                'nombre' => 'YOPAL',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => 868,
                'nombre' => 'AGUAZUL',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => 869,
                'nombre' => 'PAZ DE ARIPORO',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => 870,
                'nombre' => 'HATO COROZAL',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => 871,
                'nombre' => 'PORE',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => 872,
                'nombre' => 'RECETOR',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => 873,
                'nombre' => 'OROCUE',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => 874,
                'nombre' => 'SAN LUIS DE PALENQUE',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => 875,
                'nombre' => 'TAMARA',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => 876,
                'nombre' => 'SABANALARGA',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => 877,
                'nombre' => 'SACAMA',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => 878,
                'nombre' => 'NUNCHIA',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => 879,
                'nombre' => 'MONTERREY',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => 880,
                'nombre' => 'TAURAMENA',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => 881,
                'nombre' => 'CHAMEZA',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => 882,
                'nombre' => 'TRINIDAD',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => 883,
                'nombre' => 'MANI',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => 884,
                'nombre' => 'LA SALINA',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => 885,
                'nombre' => 'VILLANUEVA',
                'id_departamento' => 85,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => 886,
                'nombre' => 'SAN MIGUEL',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => 887,
                'nombre' => 'PUERTO GUZMAN',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => 888,
                'nombre' => 'VALLE DEL GUAMEZ-HORMIGA',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => 889,
                'nombre' => 'SIBUNDOY',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => 890,
                'nombre' => 'COLON',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => 891,
                'nombre' => 'ORITO',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => 892,
                'nombre' => 'PUERTO ASIS',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => 893,
                'nombre' => 'SANTIAGO',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => 894,
                'nombre' => 'SAN FRANCISCO',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => 895,
                'nombre' => 'PUERTO LEGUIZAMO',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => 896,
                'nombre' => 'MOCOA',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => 897,
                'nombre' => 'VILLAGARZON',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => 898,
                'nombre' => 'PUERTO CAICEDO',
                'id_departamento' => 86,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => 899,
                'nombre' => 'SAN ANDRES',
                'id_departamento' => 88,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => 900,
                'nombre' => 'PROVIDENCIA',
                'id_departamento' => 88,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => 901,
                'nombre' => 'ROBERTO PAYAN',
                'id_departamento' => 91,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => 902,
                'nombre' => 'PTO SANTANDER',
                'id_departamento' => 91,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => 903,
                'nombre' => 'TARAPACA',
                'id_departamento' => 91,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => 904,
                'nombre' => 'LA PEDRERA',
                'id_departamento' => 91,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => 905,
                'nombre' => 'EL ENCANTO',
                'id_departamento' => 91,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => 906,
                'nombre' => 'LETICIA',
                'id_departamento' => 91,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => 907,
                'nombre' => 'MIRITI-PARANA',
                'id_departamento' => 91,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => 908,
                'nombre' => 'PUERTO NARI?O',
                'id_departamento' => 91,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => 909,
                'nombre' => 'LA CHORRERA',
                'id_departamento' => 91,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => 910,
                'nombre' => 'INIRIDA',
                'id_departamento' => 94,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => 911,
                'nombre' => 'MORICHAL NUEVO',
                'id_departamento' => 94,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => 912,
                'nombre' => 'GUAVIARE-BARRANCO MINAS',
                'id_departamento' => 94,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => 913,
                'nombre' => 'SAN FELIPE',
                'id_departamento' => 94,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => 914,
                'nombre' => 'PUERTO COLOMBIA',
                'id_departamento' => 94,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => 915,
                'nombre' => 'GUADALUPE',
                'id_departamento' => 94,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => 916,
                'nombre' => 'PANA PANA',
                'id_departamento' => 94,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => 917,
                'nombre' => 'CACAHUAL',
                'id_departamento' => 94,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => 918,
                'nombre' => 'CALAMAR',
                'id_departamento' => 95,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => 919,
                'nombre' => 'EL RETORNO',
                'id_departamento' => 95,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => 920,
                'nombre' => 'SAN JOSE DEL GUAVIARE',
                'id_departamento' => 95,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => 921,
                'nombre' => 'MIRAFLORES',
                'id_departamento' => 95,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => 922,
                'nombre' => 'CARURU',
                'id_departamento' => 97,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => 923,
                'nombre' => 'YAVARATE',
                'id_departamento' => 97,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => 924,
                'nombre' => 'MITU',
                'id_departamento' => 97,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => 925,
                'nombre' => 'PAPUNAHUA',
                'id_departamento' => 97,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => 926,
                'nombre' => 'ACARICUARA',
                'id_departamento' => 97,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => 927,
                'nombre' => 'TARAIRA',
                'id_departamento' => 97,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => 928,
                'nombre' => 'VILLA FATIMA',
                'id_departamento' => 97,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => 929,
                'nombre' => 'PACOA',
                'id_departamento' => 97,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => 930,
                'nombre' => 'PUERTO CARRE?O',
                'id_departamento' => 99,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => 931,
                'nombre' => 'CUMARIBO',
                'id_departamento' => 99,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => 932,
                'nombre' => 'SAN JOSE DE OCUNE',
                'id_departamento' => 99,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => 933,
                'nombre' => 'SANTA ROSALIA',
                'id_departamento' => 99,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => 934,
                'nombre' => 'EL CEJAL',
                'id_departamento' => 99,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => 935,
                'nombre' => 'LA PRIMAVERA',
                'id_departamento' => 99,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => 936,
                'nombre' => 'SANTA RITA',
                'id_departamento' => 99,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => 937,
                'nombre' => 'LIBORINA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => 938,
                'nombre' => 'APARTADO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => 939,
                'nombre' => 'ITAGUI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => 940,
                'nombre' => 'YOLOMBO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => 941,
                'nombre' => 'ARGELIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => 942,
                'nombre' => 'DON MATIAS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => 943,
                'nombre' => 'YONDO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => 944,
                'nombre' => 'YARUMAL',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => 945,
                'nombre' => 'HISPANIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => 946,
                'nombre' => 'SAN ROQUE',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => 947,
                'nombre' => 'YALI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => 948,
                'nombre' => 'ITUANGO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => 949,
                'nombre' => 'DABEIBA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => 950,
                'nombre' => 'JARDIN',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => 951,
                'nombre' => 'ARBOLETES',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => 952,
                'nombre' => 'CHIGORODO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => 953,
                'nombre' => 'SAN RAFAEL',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => 954,
                'nombre' => 'BETULIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => 955,
                'nombre' => 'VEGACHI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => 956,
                'nombre' => 'BETANIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => 957,
                'nombre' => 'CARMEN DE VIBORAL',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => 958,
                'nombre' => 'CAROLINA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => 959,
                'nombre' => 'ANGELOPOLIS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => 960,
                'nombre' => 'HELICONIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => 961,
                'nombre' => 'SANTUARIO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => 962,
                'nombre' => 'TOLEDO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => 963,
                'nombre' => 'EBEJICO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => 964,
                'nombre' => 'ANTIOQUIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => 965,
                'nombre' => 'CAUCASIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => 966,
                'nombre' => 'ZARAGOZA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => 967,
                'nombre' => 'NECHI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => 968,
                'nombre' => 'ANDES',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => 969,
                'nombre' => 'LA ESTRELLA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => 970,
                'nombre' => 'SAN VICENTE',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => 971,
                'nombre' => 'VALDIVIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => 972,
                'nombre' => 'TARSO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => 973,
                'nombre' => 'OLAYA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => 974,
                'nombre' => 'ANGOSTURA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => 975,
                'nombre' => 'COCORNA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => 976,
                'nombre' => 'CONCORDIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => 977,
                'nombre' => 'CISNEROS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => 978,
                'nombre' => 'VENECIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => 979,
                'nombre' => 'ALEJANDRIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => 980,
                'nombre' => 'COPACABANA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => 981,
                'nombre' => 'VALPARAISO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => 982,
                'nombre' => 'SANTA ROSA DE OSOS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => 983,
                'nombre' => 'BELMIRA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => 984,
                'nombre' => 'CONCEPCION',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => 985,
                'nombre' => 'URAMITA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => 986,
                'nombre' => 'BELLO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => 987,
                'nombre' => 'TURBO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => 988,
                'nombre' => 'LA UNION',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => 989,
                'nombre' => 'RIONEGRO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => 990,
                'nombre' => 'SANTO DOMINGO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => 991,
                'nombre' => 'ABEJORRAL',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => 992,
                'nombre' => 'SANTA BARBARA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => 993,
                'nombre' => 'SAN PEDRO DE URABA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => 994,
                'nombre' => 'LA CEJA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => 995,
                'nombre' => 'ABRIAQUI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            495 => 
            array (
                'id' => 996,
                'nombre' => 'VIGIA DEL FUERTE',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            496 => 
            array (
                'id' => 997,
                'nombre' => 'REMEDIOS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            497 => 
            array (
                'id' => 998,
                'nombre' => 'URRAO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            498 => 
            array (
                'id' => 999,
                'nombre' => 'RETIRO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            499 => 
            array (
                'id' => 1000,
                'nombre' => 'ANORI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        \DB::table('tbl_municipios')->insert(array (
            0 => 
            array (
                'id' => 1001,
                'nombre' => 'BRICE?O',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 1002,
                'nombre' => 'SAN PEDRO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 1003,
                'nombre' => 'PUERTO BERRIO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 1004,
                'nombre' => 'TITIRIBI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 1005,
                'nombre' => 'SAN FRANCISCO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 1006,
                'nombre' => 'PEQUE',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 1007,
                'nombre' => 'SONSON',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 1008,
                'nombre' => 'GOMEZ PLATA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 1009,
                'nombre' => 'CALDAS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 1010,
                'nombre' => 'PE?OL',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 1011,
                'nombre' => 'CAICEDO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 1012,
                'nombre' => 'CACERES',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 1013,
                'nombre' => 'SAN CARLOS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 1014,
                'nombre' => 'AMALFI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 1015,
                'nombre' => 'SAN JERONIMO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 1016,
                'nombre' => 'MURINDO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 1017,
                'nombre' => 'SOPETRAN',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 1018,
                'nombre' => 'GIRALDO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 1019,
                'nombre' => 'AMAGA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 1020,
                'nombre' => 'SABANALARGA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 1021,
                'nombre' => 'MEDELLIN',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 1022,
                'nombre' => 'BURITICA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 1023,
                'nombre' => 'ARMENIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 1024,
                'nombre' => 'SAN ANDRES',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 1025,
            'nombre' => 'PTO NARE (LA MAGDALENA)',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 1026,
                'nombre' => 'GIRARDOTA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 1027,
                'nombre' => 'BOLIVAR',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 1028,
                'nombre' => 'SALGAR',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 1029,
                'nombre' => 'ANZA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 1030,
                'nombre' => 'MUTATA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 1031,
                'nombre' => 'SABANETA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 1032,
                'nombre' => 'CARACOLI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 1033,
                'nombre' => 'SAN LUIS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 1034,
                'nombre' => 'NARI?O',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 1035,
                'nombre' => 'EL BAGRE',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 1036,
                'nombre' => 'GUATAPE',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 1037,
                'nombre' => 'NECOCLI',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 1038,
                'nombre' => 'ENTRERRIOS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 1039,
                'nombre' => 'LA PINTADA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 1040,
                'nombre' => 'ENVIGADO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 1041,
                'nombre' => 'MONTEBELLO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 1042,
                'nombre' => 'SEGOVIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 1043,
                'nombre' => 'FRONTINO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 1044,
                'nombre' => 'GUARNE',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 1045,
                'nombre' => 'MACEO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 1046,
                'nombre' => 'CAMPAMENTO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 1047,
                'nombre' => 'CAÑASGORDAS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 1048,
                'nombre' => 'PUERTO TRIUNFO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 1049,
                'nombre' => 'MARINILLA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 1050,
                'nombre' => 'GUADALUPE',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 1051,
                'nombre' => 'TARAZA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 1052,
                'nombre' => 'BARBOSA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 1053,
                'nombre' => 'TAMESIS',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 1054,
                'nombre' => 'CARAMANTA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 1055,
                'nombre' => 'PUEBLORRICO',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 1056,
                'nombre' => 'SAN JUAN DE URABA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 1057,
                'nombre' => 'SAN JOSE DE LA MONTAÑA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 1058,
                'nombre' => 'FREDONIA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 1059,
                'nombre' => 'CAREPA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 1060,
                'nombre' => 'GRANADA',
                'id_departamento' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 1061,
                'nombre' => 'SUCRE',
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}