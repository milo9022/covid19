@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 >{{ !empty($title) ? $title : 'Users' }}</h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('users.users.index') }}" class="btn btn-primary" title="Show All Users">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('users.users.create') }}" class="btn btn-success" title="Create New Users">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('users.users.update', $users->id) }}" id="edit_users_form" name="edit_users_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="id" id="id" value="{{$users->id}}">
            <input name="_method" type="hidden" value="PUT">
            @include ('users.form', [
                                        'users' => $users,
                                      ])

                <div class="form-group">
                  
                    <div class="col-md-offset-2 col-md-10">
                        <button id="btn-save" class="btn btn-primary" type="submit"><i class="fas fa-save"></i>      Actualizar</button>
                        <button id="btn-reload" class="btn btn-warning" type="button"><i class="fas fa-sync-alt"></i>  Reiniciar contraseña</button>
                        <button id="btn-delete" class="btn btn-danger" type="button"><i class="fas fa-user-slash"></i> Bloquear usuario</button>
                    </div>
                    
                </div>
            </form>

        </div>
    </div>
<script>
$(function()
{
    $('#btn-delete').click(function()
    {
        swal({
                title: "Va a bloequear este usuario",
                text: "Si bloquea a este usuario, &#233;ste no podr&#225; acceder a la plataforma. ¿Desea continuar?",
                icon: "warning",
                buttons: ["No", "Si, borrar"],
                dangerMode: true,
            })
            .then((SiBorrar) => {
            if (SiBorrar) 
            {
                $.ajax({
                    url:base+'/usuarios/users/'+$('#id').val(),
                    type:'POST',
                    data:{'_method':'DELETE'},
                    success:function()
                    {
                        swal("El usuario ha sido borrado con &#233;xito", {icon: "success"});
                        window.location.href = base+"/usuarios";
                    }
                })
            } else {
                swal("El usuario NO ha sido borrado");
            }
        });
    })
    $('#btn-reload').click(function()
    {
        $.ajax({
            url:base+'/usuarios/restablecercontrasenna',
            type:'POST',
            data:{id:$('#id').val()},
            dataType:'json',
            success:function(data)
            {
                swal("Contraseña reestablecida", "La contraseña ser&#225; por defecto el numero de documento", "success");
            }
        })
    })

})
</script>
@endsection