<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model;

/**
 * Class TblCuestionarioXRespuestum
 * 
 * @property int $id
 * @property int $id_cuestionario
 * @property int $id_pregunta
 * @property string $value
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property TblCuestionario $tbl_cuestionario
 * @property TblPregunta $tbl_pregunta
 *
 * @package App\Models
 */
class TblCuestionarioXRespuestum extends Model
{
	protected $table = 'tbl_cuestionario_x_respuesta';

	protected $casts = [
		'id_cuestionario' => 'int',
		'id_pregunta' => 'int'
	];

	protected $fillable = [
		'id_cuestionario',
		'id_pregunta',
		'value'
	];

	public function tbl_cuestionario()
	{
		return $this->belongsTo(TblCuestionario::class, 'id_cuestionario');
	}

	public function tbl_pregunta()
	{
		return $this->belongsTo('App\Models\tbl_preguntas', 'id_pregunta');
	}
}
