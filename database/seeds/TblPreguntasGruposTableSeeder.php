<?php

use Illuminate\Database\Seeder;

class TblPreguntasGruposTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_preguntas_grupos')->delete();
        
        \DB::table('tbl_preguntas_grupos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'ANTECEDENTES DE RIESGO Y EXPOSICION',
                'created_at' => '2020-03-20 10:57:53',
                'updated_at' => '2020-03-20 10:58:04',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'SIGNOS Y SINTOMAS AL INGRESO',
                'created_at' => '2020-03-20 10:58:19',
                'updated_at' => '2020-03-20 10:58:19',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'COMORBILIDADES/ FACTORES DE RIESGO',
                'created_at' => '2020-03-20 10:58:34',
                'updated_at' => '2020-03-20 10:58:34',
            ),
        ));
        
        
    }
}