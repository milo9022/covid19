<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblPuntosAtencion extends Model
{
    protected $table = 'tbl_puntos_atencion';

	protected $fillable = ['nombre'];
}
