<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model;

/**
 * Class TblMunicipio
 * 
 * @property int $id
 * @property string $nombre
 * @property int $id_departamento
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblMunicipio extends Model
{
	protected $table = 'tbl_municipios';

	protected $casts = [
		'id_departamento' => 'int'
	];

	protected $fillable = [
		'nombre',
		'id_departamento'
	];
}
