@extends('layouts.app')

@section('content')
    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Respuestas</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <button class="btn btn-success"  onclick="exportTableToCSV()">
                <i class="fas fa-download"></i>
                <i class="fas fa-file-csv"></i>
                </button>
            </div>

        </div>
        
        @if(count($respuestas) == 0)
            <div class="panel-body text-center">
                <h4>No Respuestas Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped " borde="1">
                    <thead>
                        <tr>
                            <th style="min-width: 100px;">FECHA DE REGISTRO</th> 	
                            <th>TIPO DE IDENTIFICACION</th> 	
                            <th>NUMERO DE IDENTIFICACION
                            <th>PRIMER NOMBRE</th>
                            <th>SEGUNDO NOMBRE</th>
                            <th>PRIMER APELLIDO</th>
                            <th>SEGUNDO APELLIDO</th>
                            <th>GENERO</th>
                            <th>FECHA DE NACIMIENTO</th>
                            <th style="min-width:100px">EDAD</th>
                            <th>UNIDAD DE MEDIDA</th> 	
                            <th>NACIONALIDAD</th>
                            <th>DOMICILIO</th>
                            <th>DIRECCION</th>
                            <th>BARRIO /VEREDA</th>	
                            <th>ZANA RESIDENCIA</th>
                            <th>TELEFONO</th>
                            <th>NOMBRE DE EPS</th>
                            <th>OCUPACION</th>
                            @for($i=0;$i!=1;$i++)
                            @foreach(($respuestas[0])->tbl_cuestionario_x_respuesta as $temp2)
                            <th style="min-width:250px">
                            {{$temp2->tbl_pregunta->nombre}}
                            </th>
                            @endforeach
                            @endfor	
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($respuestas as $key=> $respuesta)
                        <tr>
                        <td>{{date('Y-m-d h:i:s A',strtotime($respuesta->created_at))}}</td>
                        <td>{{isset($respuesta->tblPaciente->tbl_documento_tipo->nombre)?$respuesta->tblPaciente->tbl_documento_tipo->nombre:''}}</td>
                        <td>{{$respuesta->tblPaciente->documento}}</td>

                        <td>{{$respuesta->tblPaciente->nombre_primero}}</td>
                        <td>{{$respuesta->tblPaciente->nombre_segundo}}</td>
                        <td>{{$respuesta->tblPaciente->apellido_primero}}</td>
                        <td>{{$respuesta->tblPaciente->apellido_segundo}}</td>
                        <td>{{$respuesta->tblPaciente->genero}}</td>
                        <td>{{date('Y-m-d',strtotime($respuesta->tblPaciente->fecha_nacimiento))}}</td>
                        <td>
                        <?php
                        $fecha_nac = new DateTime(date('Y/m/d',strtotime($respuesta->tblPaciente->fecha_nacimiento))); // Creo un objeto DateTime de la fecha ingresada
                        $fecha_hoy =  new DateTime(date('Y/m/d',time())); // Creo un objeto DateTime de la fecha de hoy
                        $edad = date_diff($fecha_hoy,$fecha_nac);
                        echo "{$edad->format('%Y')} años y {$edad->format('%m')} meses";
                        ?>
                        </td>
                        <td>-</td>
                        <td>-</td>
                        <td>{{$respuesta->tblPaciente->domicilio}}</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>{{$respuesta->tblPaciente->celular1}}</td>
                        <td>-</td>
                        <td>-</td>
                        @foreach($respuesta->tbl_cuestionario_x_respuesta as $temp2)
                        <td>
                        {{$temp2->value}}
                        </td>
                        @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        @endif
    </div>
    <script>
function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;
    csvFile = new Blob([csv], {type: "text/csv"});
    downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

function exportTableToCSV()
{
    var filename = 'registros.csv';
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    for (var i = 0; i < rows.length; i++) 
    {
        var row = [], cols = rows[i].querySelectorAll("td, th");    
        for (var j = 0; j < cols.length; j++) 
        {
            row.push(cols[j].innerText);
        }
        csv.push(row.join(","));        
    }
    downloadCSV(csv.join("\n"), filename);
}
    </script>
@endsection