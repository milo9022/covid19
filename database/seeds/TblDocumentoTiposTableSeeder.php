<?php

use Illuminate\Database\Seeder;

class TblDocumentoTiposTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_documento_tipos')->delete();
        
        \DB::table('tbl_documento_tipos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Registro civil',
                'nombre_corto' => 'RC',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Tarjeta de identidad',
                'nombre_corto' => 'TI',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Cédula de ciudadanía',
                'nombre_corto' => 'CC',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Cédula de extrangería',
                'nombre_corto' => 'CE',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'AS',
                'nombre_corto' => 'AS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'MS',
                'nombre_corto' => 'MS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'PE',
                'nombre_corto' => 'PE',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'PA',
                'nombre_corto' => 'PA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'ASI',
                'nombre_corto' => 'ASI',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'MSI',
                'nombre_corto' => 'MSI',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'NV',
                'nombre_corto' => 'NV',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'NU',
                'nombre_corto' => 'NU',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}