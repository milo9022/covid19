<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblCuestionarioXRespuestaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbl_cuestionario_x_respuesta', function(Blueprint $table)
		{
			$table->foreign('id_cuestionario', 'fk_tbl_cuestionario_tbl_cuestionario_x_respuesta')->references('id')->on('tbl_cuestionario')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_pregunta', 'fk_tbl_preguntas_tbl_cuestionario_x_respuesta')->references('id')->on('tbl_preguntas')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbl_cuestionario_x_respuesta', function(Blueprint $table)
		{
			$table->dropForeign('fk_tbl_cuestionario_tbl_cuestionario_x_respuesta');
			$table->dropForeign('fk_tbl_preguntas_tbl_cuestionario_x_respuesta');
		});
	}

}
