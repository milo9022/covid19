<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\tbl_preguntas_grupos;
use Illuminate\Http\Request;
use Exception;

class TblPreguntasGruposController extends Controller
{

    /**
     * Display a listing of the tbl preguntas grupos.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $tblPreguntasGruposObjects = tbl_preguntas_grupos::with('tblPreguntas')->paginate(25);

        return view('tbl_preguntas_grupos.index', compact('tblPreguntasGruposObjects'));
    }
    public function all()
    {
        return tbl_preguntas_grupos::with('tblPreguntas')->get();
    }
    /**
     * Show the form for creating a new tbl preguntas grupos.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('tbl_preguntas_grupos.create');
    }

    /**
     * Store a new tbl preguntas grupos in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            tbl_preguntas_grupos::create($data);

            return redirect()->route('tbl_preguntas_grupos.tbl_preguntas_grupos.index')
                ->with('success_message', 'Tbl Preguntas Grupos was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Error: '.$exception->getMessage()]);
        }
    }

    /**
     * Display the specified tbl preguntas grupos.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblPreguntasGrupos = tbl_preguntas_grupos::findOrFail($id);

        return view('tbl_preguntas_grupos.show', compact('tblPreguntasGrupos'));
    }

    /**
     * Show the form for editing the specified tbl preguntas grupos.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tblPreguntasGrupos = tbl_preguntas_grupos::findOrFail($id);
        

        return view('tbl_preguntas_grupos.edit', compact('tblPreguntasGrupos'));
    }

    /**
     * Update the specified tbl preguntas grupos in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblPreguntasGrupos = tbl_preguntas_grupos::findOrFail($id);
            $tblPreguntasGrupos->update($data);

            return redirect()->route('tbl_preguntas_grupos.tbl_preguntas_grupos.index')
                ->with('success_message', 'Tbl Preguntas Grupos was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified tbl preguntas grupos from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblPreguntasGrupos = tbl_preguntas_grupos::findOrFail($id);
            $tblPreguntasGrupos->delete();

            return redirect()->route('tbl_preguntas_grupos.tbl_preguntas_grupos.index')
                ->with('success_message', 'Tbl Preguntas Grupos was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'nombre' => 'required|string|min:1|max:191',
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
