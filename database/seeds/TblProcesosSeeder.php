<?php

use Illuminate\Database\Seeder;

class TblProcesosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tbl_procesos')->delete();
        
        \DB::table('tbl_procesos')->insert(array ( 
            array (
                'nombre'=>'Asignación de citas',
                'created_at' => date('Y-m-d h:i:s')
            ),

            )
        );
    }
}
