
<div class="form-group {{ $errors->has('nombre_primero') ? 'has-error' : '' }}">
    <label for="nombre_primero" class="col-md-2 control-label">Nombre Primero</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre_primero" type="text" id="nombre_primero" value="{{ old('nombre_primero', optional($users)->nombre_primero) }}" minlength="1" maxlength="191" required="true" "Enter nombre primero here...">
        {!! $errors->first('nombre_primero', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('nombre_segundo') ? 'has-error' : '' }}">
    <label for="nombre_segundo" class="col-md-2 control-label">Nombre Segundo</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre_segundo" type="text" id="nombre_segundo" value="{{ old('nombre_segundo', optional($users)->nombre_segundo) }}" minlength="1" maxlength="191" required="true" "Enter nombre segundo here...">
        {!! $errors->first('nombre_segundo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('apellido_primero') ? 'has-error' : '' }}">
    <label for="apellido_primero" class="col-md-2 control-label">Apellido Primero</label>
    <div class="col-md-10">
        <input class="form-control" name="apellido_primero" type="text" id="apellido_primero" value="{{ old('apellido_primero', optional($users)->apellido_primero) }}" minlength="1" maxlength="191" required="true" "Enter apellido primero here...">
        {!! $errors->first('apellido_primero', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('apellido_segundo') ? 'has-error' : '' }}">
    <label for="apellido_segundo" class="col-md-2 control-label">Apellido Segundo</label>
    <div class="col-md-10">
        <input class="form-control" name="apellido_segundo" type="text" id="apellido_segundo" value="{{ old('apellido_segundo', optional($users)->apellido_segundo) }}" minlength="1" maxlength="191" required="true" "Enter apellido segundo here...">
        {!! $errors->first('apellido_segundo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('documento') ? 'has-error' : '' }}">
    <label for="documento" class="col-md-2 control-label">Documento</label>
    <div class="col-md-10">
        <input class="form-control" name="documento" type="text" id="documento" value="{{ old('documento', optional($users)->documento) }}" minlength="1" maxlength="191" required="true" "Enter documento here...">
        {!! $errors->first('documento', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_punto_atencion') ? 'has-error' : '' }}">
    <label for="id_punto_atencion" class="col-md-2 control-label">Punto de Atencion asignado</label>
    <div class="col-md-10">
        <select class="form-control" id="id_punto_atencion" name="id_punto_atencion" required="true">
        	    <option value="" style="display: none;" {{ old('id_punto_atencion', optional($users)->id_punto_atencion ?: '') == '' ? 'selected' : '' }} disabled selected>Ingrese el punto de atenci&#243n</option>
                @foreach ($TblPuntosAtenciones as $key => $TblPuntosAtencion)
                    <option value="{{ $key }}" {{ old('id_punto_atencion', optional($users)->id_punto_atencion) == $key ? 'selected' : '' }}>
                        {{ $TblPuntosAtencion }}
                    </option>
                @endforeach
        </select>
        
        
        
        {!! $errors->first('id_punto_atencion', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('id_rol') ? 'has-error' : '' }}">
    <label for="id_rol" class="col-md-2 control-label">Perfil del usuario</label>
    <div class="col-md-10">
        <select class="form-control" id="id_rol" name="id_rol" required="true">
        	    <option value="" style="display: none;" {{ old('id_rol', optional($users)->id_rol ?: '') == '' ? 'selected' : '' }} disabled selected>Ingrese el perfil del usuario</option>
                @foreach ($TblRoles as $key => $TblRol)
                    <option value="{{ $key }}" {{ old('id_rol', optional($users)->id_rol) == $key ? 'selected' : '' }}>
                        {{ $TblRol }}
                    </option>
                @endforeach
        </select>
        
        
        
        {!! $errors->first('id_rol', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <label for="email" class="col-md-2 control-label">Email</label>
    <div class="col-md-10">
        <input class="form-control" name="email" type="email" id="email" value="{{ old('email', optional($users)->email) }}" minlength="1" maxlength="191" required="true" "Enter email here...">
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

