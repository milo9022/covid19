<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model;

/**
 * Class TblDocumentoTipo
 * 
 * @property int $id
 * @property string $nombre
 * @property string $nombre_corto
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblDocumentoTipo extends Model
{
	protected $table = 'tbl_documento_tipos';

	protected $fillable = [
		'nombre',
		'nombre_corto'
	];
}
