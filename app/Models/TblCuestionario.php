<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Reliese\Database\Eloquent\Model;

/**
 * Class TblCuestionario
 * 
 * @property int $id
 * @property int $id_paciente
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Collection|TblCuestionarioXRespuestum[] $tbl_cuestionario_x_respuesta
 *
 * @package App\Models
 */
class TblCuestionario extends Model
{
	protected $table = 'tbl_cuestionario';

	protected $casts = [
		'id_paciente' => 'int'
	];

	protected $fillable = [
		'id_paciente','id_user'
	];

	public function tbl_cuestionario_x_respuesta()
	{
		return $this->hasMany(TblCuestionarioXRespuestum::class, 'id_cuestionario');
	}
	public function tblPaciente()
    {
        return $this->belongsTo(TblPaciente::class, 'id_paciente');
	}
	public function tblUser()
    {
        return $this->belongsTo(User::class, 'id_user');
	}
	
}
