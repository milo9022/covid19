
<div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
    <label for="nombre" class="col-md-2 control-label">Nombre</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre" type="text" id="nombre" value="{{ old('nombre', optional($tblPreguntas)->nombre) }}" minlength="1" maxlength="191" required="true" placeholder="Enter nombre here...">
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('nivel') ? 'has-error' : '' }}">
    <label for="nivel" class="col-md-2 control-label">Tipo</label>
    <div class="col-md-10">
    <select name="" id="" class="form form-control">
        <option value="1">Si/No</option>
        <option value="2">Texto</option>
    </select>
        <input class="form-control" name="nivel" type="number" id="nivel" value="{{ old('nivel', optional($tblPreguntas)->nivel) }}" min="-2147483648" max="2147483647" placeholder="Enter nivel here...">
        {!! $errors->first('nivel', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_preguntas_grupos') ? 'has-error' : '' }}">
    <label for="id_preguntas_grupos" class="col-md-2 control-label">Preguntas Grupos</label>
    <div class="col-md-10">
    <select class="form form-control" name="id_preguntas_grupos" id="id_preguntas_grupos" required="true">
    @foreach($tblPreguntasGruposObjects as $temp)
        <option value="{{$temp->id}}">{{$temp->nombre}}</option>
    @endforeach
    </select>
        <input style="display:none" class="form-control" type="text" value="{{ old('id_preguntas_grupos', optional($tblPreguntas)->id_preguntas_grupos) }}" >
        {!! $errors->first('id_preguntas_grupos', '<p class="help-block">:message</p>') !!}
    </div>
</div>

