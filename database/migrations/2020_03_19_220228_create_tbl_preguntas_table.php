<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTblPreguntasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbl_preguntas', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('nombre');
			$table->integer('nivel')->nullable();
			$table->bigInteger('id_preguntas_grupos')->unsigned()->index('fk_preguntas_grupos');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbl_preguntas');
	}

}
